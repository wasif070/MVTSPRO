jQuery(document).ready(function(){
    clients.init();
    rates.init();
    $(".jsel_all").change(function(){
        if($(this).attr('checked')){
            $('.select_id').attr('checked','checked');
        }else{
            $('.select_id').attr('checked','');
        }
    });
});

var clients={
    init:function(){
        $("#jgateway_type").change(function(){
            clients.load_client(this);
        });
        
        $('.jmultipebtn').click(function(){
            var allVals = [];
            $('.select_id:checked').each(function() {
                allVals.push($(this).val());
            });
            
            if(allVals.length <=0){
                $('.jerror_messge').html("<div class='error'>Please Select at least "+$('.jsel_all').attr('title')+"!</div>")
                return false;
            }else{
                return true;
            }
        });
        $('.jclient_id').keyup(function(){
            clients.get_client_list(this);
        });
        
        $('.jtermClient_id').keyup(function(){
            clients.get_termClient_list(this);
        });
        
        $('.joriginId').change(function(){
            var client_id=$(this).val();
            clients.load_gatewayip(client_id);
            $('.joriginIp').attr('disabled',false);
            $('.jorigindest').attr('disabled',false);
        });
        
        $('.jtermId').change(function(){
            var client_id=$(this).val();
            clients.load_termgatewayip(client_id);
            $('.jtermIp').attr('disabled',false);
            $('.jtermdest').attr('disabled',false);
        });
        
    },
    load_client:function(obj){
        var gateway_type=$(obj).val();
        $.ajax({
            type:'post',
            url:'../gateway/load_client.jsp',
            data:{
                type:gateway_type
            },
            success:function(html){
                $("#jclientId").html(html);
            }
        });
    },
    bind:function(){
        $('.jclient_list').find('li').unbind('click');
        $('.jclient_list').find('li').bind('click',function(){
            $('.jclient_id').val($(this).attr('title'));
            var client_id=$(this).attr('rel');
            $('.joriginId').val(client_id);
            clients.load_gatewayip(client_id);
            $('.joriginIp').attr('disabled',false);
            $('.jorigindest').attr('disabled',false);
            $('.jclient_list').css('display','none');
        });
    }
    ,
    get_client_list:function(obj){
        $('.jclient_list').css('display','block');
        var search=$('.jclient_id').val();
        var type= $(obj).attr('title');
        $.ajax({
            type:'post',
            url:'../mvtscdr/load_client.jsp',
            data:{
                search:search,
                type: type
            },
            success:function(html){
                $('.jclient_list').html(html);
                clients.bind();
            },
            error:function(a,b,c){
            //alert(a+'\n'+b+'\n'+c);
            }
        });
    },
    load_gatewayip:function(client_id){
        $.ajax({
            type:'post',
            url:'../mvtscdr/load_gateway.jsp',
            data:{
                client_id:client_id,
                type: 0
            },
            success:function(html){
                $('.joriginIp').html(html);
            },
            error:function(e,m,s){
            //alert(e+m+s);
            }
        });
    },
    bind_term:function(){
        $('.jtermclient_list').find('li').unbind('click');
        $('.jtermclient_list').find('li').bind('click',function(){
            $('.jtermClient_id').val($(this).attr('title'));
            var client_id=$(this).attr('rel');
            $('.jtermId').val(client_id);
            clients.load_termgatewayip(client_id);
            $('.jtermIp').attr('disabled',false);
            $('.jtermdest').attr('disabled',false);
            $('.jtermclient_list').css('display','none');
        });
    }
    ,
    get_termClient_list:function(obj){
        $('.jtermclient_list').css('display','block');
        var search=$('.jtermclient_id').val();
        var type= $(obj).attr('title');
        $.ajax({
            type:'post',
            url:'../mvtscdr/load_client.jsp',
            data:{
                search:search,
                type: type
            },
            success:function(html){
                $('.jtermclient_list').html(html);
                clients.bind_term();
            },
            error:function(a,b,c){
            //alert(a+'\n'+b+'\n'+c);
            }
        });
    },
    load_termgatewayip:function(client_id){
        $.ajax({
            type:'post',
            url:'../mvtscdr/load_gateway.jsp',
            data:{
                client_id:client_id,
                type: 1
            },
            success:function(html){
                $('.jtermIp').html(html);
            },
            error:function(e,m,s){
            //alert(e+m+s);
            }
        });
    }
}
var rates={
    init:function(){
        $('.jadd_more_rate').live('click',function(){
            rates.load_rate_options();
        });
    },
    load_rate_options:function(){
        $.ajax({
            type:'post',
            url:'../rates/load_rate_options.jsp',
            data:{
            },
            success:function(html){
                $('.more_option').replaceWith(html);
            },
            error:function(e,m,s){
            //alert(e+m+s);
            }
        });
    }
}