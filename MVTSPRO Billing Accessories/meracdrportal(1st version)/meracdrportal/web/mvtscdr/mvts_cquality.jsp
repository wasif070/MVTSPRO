<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.mvtscdr.MvtsQualityDTO,com.myapp.struts.mvtscdr.MvtsCdrDTO,com.myapp.struts.mvtscdr.MvtsCdrDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
    if (msg == null) {
        msg = "";
    } else {
        msg = "<div class='error' style='width:700px;'>" + msg + "</div>";
    }
    String summaryBy = (String) request.getParameter("sby");
    if (summaryBy == null || summaryBy.length() < 1) {
        summaryBy = "0,1,2,3";
    } else {
        summaryBy = summaryBy.substring(0, summaryBy.length() - 1);
    }

    ArrayList<Integer> days = Utils.getDay();
    ArrayList<String> months = Utils.getMonth();
    ArrayList<Integer> years = Utils.getYear();
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> minsec = Utils.getTimeValue(60);
    ArrayList<MvtsQualityDTO> mvtscdrList = (ArrayList<MvtsQualityDTO>) request.getSession(true).getAttribute("MvtsCdrDTO");
    NumberFormat formatter = new DecimalFormat("00");
    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
    int total_time = 0;
    int total_success = 0;
    int total_fail = 0;
    String bg_class = "odd";
    NumberFormat pddformatter = new DecimalFormat("#00.00");

%>
<html>
    <head>
        <title>24Billing :: Mvts Cdr Quality Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div class="top"></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right" style="width:80%">
                <div class="pad_10 border_left">
                    <%
                        switch (login_dto.getClientType()) {
                            case Constants.BOTH:
                    %>
                    <html:form action="/mvtscdr/mvtsCQuality.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:700px;" border="0" cellpadding="0" cellspacing="0">
                                <tr><td colspan="2" class="center-align bold">Origination</td><td align="center" class="center-align bold" colspan="2">Termination</td></tr>
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origination_ip"  styleClass="joriginIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                if (clientGateway != null && clientGateway.size() > 0) {
                                                    int size = clientGateway.size();
                                                    for (int i = 0; i < size; i++) {
                                                        GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                        if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>
                                            <% }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                    <th>Termination  IP</th>
                                    <td><html:select property="termination_ip" styleClass="jtermIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                //ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
                                                if (clientGateway != null && clientGateway.size() > 0) {
                                                    int size = clientGateway.size();
                                                    for (int i = 0; i < size; i++) {
                                                        GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                        if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>
                                            <% }
                                                    }
                                                }

                                            %>
                                        </html:select></td>
                                </tr>
                                <tr>
                                    <th>Destination</th>
                                    <td><html:text property="originDestination" styleClass="jorigindest" /></td>
                                    <th>Destination</th>
                                    <td><html:text property="termDestination" styleClass="jtermdest" /></td>
                                </tr>
                                <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Year</div><div class="fl_left month">Month</div><div class="fl_left">Day</div><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                <tr>
                                    <th>From Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>To Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Summary By</th>
                                    <td colspan="3">
                                        <%
                                            for (int i = 0; i < Constants.SUMMARY_VALUE.length; i++) {
                                        %>
                                        <input type="checkbox" name="summaryBy[]" <% if (summaryBy.contains(Constants.SUMMARY_VALUE[i])) {%> checked="checked" <% }%> value="<%=Constants.SUMMARY_VALUE[i]%>" /><%=Constants.SUMMARY_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Unit Time </th>
                                    <td colspan="3">
                                        <%
                                            for (int i = 0; i < Constants.UNIT_TIME_VALUE.length; i++) {
                                        %>
                                        <html:radio property="summaryType" value="<%=Constants.UNIT_TIME_VALUE[i]%>" /><%=Constants.UNIT_TIME_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>
                    <div class="over_flow_content">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="3%">Nr.</th>
                                    <th rowspan="2" width="7">Period start time</th>
                                    <th rowspan="2" width="7">Period End time</th>
                                    <th colspan="3" width="23%">Origination</th>
                                    <th colspan="3" width="22%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Success / Fail</th>
                                    <th rowspan="2" width="5%">ASR(%)</th>
                                    <th rowspan="2" width="5%">ACD (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Avg PDD(Sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Destination</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            double avg_pdd = 0;
                                            int acd = 0;
                                            MvtsQualityDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getDuration();
                                            total_success += obj.getTotal_success();
                                            total_fail += obj.getTotal_fail();
                                            int total_call = obj.getTotal_success() + obj.getTotal_fail();
                                            if (obj.getTotal_success() > 0) {
                                                acd = (int) (obj.getDuration() / obj.getTotal_success());
                                            }

                                            float success_rate = 0;
                                            if (total_call > 0) {
                                                avg_pdd = (double) (obj.getAvg_pdd() / total_call);
                                                success_rate = (float) (obj.getTotal_success() * 100 / total_call);
                                            }


                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_start())%></td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_end())%></td>
                                    <td><%=obj.getOriginClient()%></td>
                                    <td><%=obj.getOrigination_ip()%></td>
                                    <td><%=obj.getOrigin_dest()%></td>
                                    <td><%=obj.getTermClient()%></td>
                                    <td><%=obj.getTermination_ip()%></td>
                                    <td><%=obj.getTerm_dest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=obj.getTotal_success() %> / <%=obj.getTotal_fail() %></td>
                                    <td><%=pddformatter.format(success_rate)%></td>
                                    <td><%=Utils.getTimeMMSS(acd)%></td>
                                    <td><%=pddformatter.format(avg_pdd)%></td>
                                </tr>
                                <%   }%>
                                <tr>
                                    <th colspan="9">Total: </th>
                                    <th align="center"><%=Utils.getTimeMMSS(total_time)%></th>
                                    <td align="center"><%=total_success%>/<%=total_fail%></td>
                                    <td align="center"><%=pddformatter.format(((double)total_success/(double)(total_success+total_fail))*100) %></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <%   } else {%>
                                <tr><th colspan="14">No Content Found!</th></tr>
                                <% }%>
                            </tbody>
                        </table>
                    </div>
                    <% break;
                        case Constants.ORIGINATION:%>
                    <html:form action="/mvtscdr/mvtsCQuality.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:700px;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origination_ip"  styleClass="joriginIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                if (clientGateway != null && clientGateway.size() > 0) {
                                                    int size = clientGateway.size();
                                                    for (int i = 0; i < size; i++) {
                                                        GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                        if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>
                                            <% }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                </tr>
                                <tr>
                                    <th>Destination</th>
                                    <td><html:text property="originDestination" styleClass="jorigindest" /></td>
                                </tr>
                                <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Year</div><div class="fl_left month">Month</div><div class="fl_left">Day</div><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>To Date</th>
                                    <td class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Summary By</th>
                                    <td>
                                        <%
                                            for (int i = 0; i < Constants.SUMMARY_VALUE.length - 2; i++) {
                                        %>
                                        <input type="checkbox" name="summaryBy[]" <% if (summaryBy.contains(Constants.SUMMARY_VALUE[i])) {%> checked="checked" <% }%> value="<%=Constants.SUMMARY_VALUE[i]%>" /><%=Constants.SUMMARY_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Unit Time </th>
                                    <td colspan="3">
                                        <%
                                            for (int i = 0; i < Constants.UNIT_TIME_VALUE.length; i++) {
                                        %>
                                        <html:radio property="summaryType" value="<%=Constants.UNIT_TIME_VALUE[i]%>" /><%=Constants.UNIT_TIME_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>
                    <div>
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="3%">Nr.</th>
                                    <th rowspan="2" width="7">Period start time</th>
                                    <th rowspan="2" width="7">Period End time</th>
                                    <th colspan="3" width="23%">Origination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="5">Success / Fail</th>
                                    <th rowspan="2" width="5">ASR(%)</th>
                                    <th rowspan="2" width="5">ACD (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Avg PDD(Sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            double avg_pdd = 0;
                                            int acd = 0;
                                            MvtsQualityDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getDuration();
                                            total_success += obj.getTotal_success();
                                            total_fail += obj.getTotal_fail();
                                            int total_call = obj.getTotal_success() + obj.getTotal_fail();
                                            if (obj.getTotal_success() > 0) {
                                                acd = (int) (obj.getDuration() / obj.getTotal_success());
                                            }

                                            float success_rate = 0;
                                            if (total_call > 0) {
                                                avg_pdd = (double) (obj.getAvg_pdd() / total_call);
                                                success_rate = (float) (obj.getTotal_success() * 100 / total_call);
                                            }


                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_start())%></td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_end())%></td>
                                    <td><%=obj.getOriginClient()%></td>
                                    <td><%=obj.getOrigination_ip()%></td>
                                    <td><%=obj.getOrigin_dest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=obj.getTotal_success()%> / <%=obj.getTotal_fail()%></td>
                                    <td><%=pddformatter.format(success_rate)%></td>
                                    <td><%=Utils.getTimeMMSS(acd)%></td>
                                    <td><%=pddformatter.format(avg_pdd)%></td>
                                </tr>
                                <%   }%>
                                <tr>
                                    <th colspan="6">Total: </th>
                                    <th align="center"><%=Utils.getTimeMMSS(total_time)%></th>
                                    <td align="center"><%=total_success%>/<%=total_fail%></td>
                                    <td align="center"><%=pddformatter.format(((double)total_success/(double)(total_success+total_fail))*100) %></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <%   } else {%>
                                <tr><th colspan="11">No Content Found!</th></tr>
                                <% }%>
                            </tbody>
                        </table>
                    </div>
                    <%  break;
                        case Constants.TERMINATION:%>
                    <html:form action="/mvtscdr/mvtsCQuality.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:700px;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Termination  IP</th>
                                    <td><html:select property="termination_ip" styleClass="jtermIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                if (clientGateway != null && clientGateway.size() > 0) {
                                                    int size = clientGateway.size();
                                                    for (int i = 0; i < size; i++) {
                                                        GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                        if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>
                                            <% }
                                                    }
                                                }

                                            %>
                                        </html:select></td>
                                </tr>
                                <tr>
                                    <th>Destination</th>
                                    <td><html:text property="termDestination" styleClass="jtermdest" /></td>
                                </tr>
                                <tr><th>&nbsp;</th><td class="selopt"><div class="fl_left">Year</div><div class="fl_left month">Month</div><div class="fl_left">Day</div><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>To Date</th>
                                    <td class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Summary By</th>
                                    <td>
                                        <%
                                            for (int i = 2; i < Constants.SUMMARY_VALUE.length; i++) {
                                        %>
                                        <input type="checkbox" name="summaryBy[]" <% if (summaryBy.contains(Constants.SUMMARY_VALUE[i])) {%> checked="checked" <% }%> value="<%=Constants.SUMMARY_VALUE[i]%>" /><%=Constants.SUMMARY_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Unit Time </th>
                                    <td>
                                        <%
                                            for (int i = 0; i < Constants.UNIT_TIME_VALUE.length; i++) {
                                        %>
                                        <html:radio property="summaryType" value="<%=Constants.UNIT_TIME_VALUE[i]%>" /><%=Constants.UNIT_TIME_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>
                    <div>
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="3%">Nr.</th>
                                    <th rowspan="2" width="7">Period start time</th>
                                    <th rowspan="2" width="7">Period End time</th>
                                    <th colspan="3" width="22%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Success / Fail</th>
                                    <th rowspan="2" width="5%">ASR(%)</th>
                                    <th rowspan="2" width="5%">ACD (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Avg PDD(Sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            double avg_pdd = 0;
                                            int acd = 0;
                                            MvtsQualityDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getDuration();
                                            total_success += obj.getTotal_success();
                                            total_fail += obj.getTotal_fail();
                                            int total_call = obj.getTotal_success() + obj.getTotal_fail();
                                            if (obj.getTotal_success() > 0) {
                                                acd = (int) (obj.getDuration() / obj.getTotal_success());
                                            }

                                            float success_rate = 0;
                                            if (total_call > 0) {
                                                avg_pdd = (double) (obj.getAvg_pdd() / total_call);
                                                success_rate = (float) (obj.getTotal_success() * 100 / total_call);
                                            }


                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_start())%></td>
                                    <td><%=Utils.LongToDate(obj.getPeriod_end())%></td>
                                    <td><%=obj.getTermClient()%></td>
                                    <td><%=obj.getTermination_ip()%></td>
                                    <td><%=obj.getTerm_dest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=obj.getTotal_success()%> / <%=obj.getTotal_fail()%></td>
                                    <td><%=pddformatter.format(success_rate)%></td>
                                    <td><%=Utils.getTimeMMSS(acd)%></td>
                                    <td><%=pddformatter.format(avg_pdd)%></td>
                                </tr>
                                <%   }%>
                                <tr>
                                    <th colspan="6">Total: </th>
                                    <th align="center"><%=Utils.getTimeMMSS(total_time)%></th>
                                    <td align="center"><%=total_success%>/<%=total_fail%></td>
                                    <td align="center"><%=pddformatter.format(((double)total_success/(double)(total_success+total_fail))*100) %></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <%   } else {%>
                                <tr><th colspan="11">No Content Found!</th></tr>
                                <% }%>
                            </tbody>
                        </table>
                    </div>
                    <% break;
                        }%>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>