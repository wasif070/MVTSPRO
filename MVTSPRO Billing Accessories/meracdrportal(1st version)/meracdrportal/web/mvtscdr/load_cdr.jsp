<%@page import="com.myapp.struts.util.Utils,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.mvtscdr.MvtsCdrDTO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.mvtscdr.MvtsCdrDAO" %>
<%
    MvtsCdrDAO dao= new MvtsCdrDAO();
    ArrayList<MvtsCdrDTO> mvtscdrList = dao.getActiveMvtsCdrDTOList(0,Constants.PER_PAGE_RECORD);
    NumberFormat formatter = new DecimalFormat("00");
    int total_time = 0;

%>
<tbody id="jreload_cdr">
    <%
        String bg_class = "odd";
        if (mvtscdrList != null && mvtscdrList.size() > 0) {
            for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                MvtsCdrDTO obj = mvtscdrList.get(inc);
                if (inc % 2 == 0) {
                    bg_class = "even";
                } else {
                    bg_class = "odd";
                }

                total_time += obj.getElapsed_time();
    %>
    <tr class="<%=bg_class%> center-align">
        <td align="right"><%=inc + 1%>.</td>
        <td><%=obj.getIn_dnis()%></td>
        <td><%=obj.getIn_ani()%></td>
        <td><%=obj.getOriginClient_id()%></td>
        <td><%=obj.getRemote_src_sig_address()%></td>
        <td><%=obj.getOriginClient_prefix()%></td>
        <td><%=obj.getOriginDest()%></td>
        <td><%=obj.getOut_ani()%></td>
        <td><%=obj.getTermClient_id()%></td>
        <td><%=obj.getRemote_dst_sig_address()%></td>
        <td><%=obj.getTermClient_prefix()%></td>
        <td><%=obj.getTermDest()%></td>
        <td><%=Utils.getTimeMMSS(obj.getElapsed_time())%></td>
        <td><%=obj.getConnect_time()%></td>
        <td><%=obj.getPdd()%></td>
    </tr>
    <%   }%>
    <tr><th colspan="12">Total: </th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td></tr>
    <%   } else {%>
    <tr><th colspan="15">No Content Found!</th></tr>
    <% }
    %>
</tbody>