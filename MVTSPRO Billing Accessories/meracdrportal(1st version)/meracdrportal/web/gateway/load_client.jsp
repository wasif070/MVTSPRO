<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO" %>
<%
    int type = 0;
    String output = "";
    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }

    ArrayList<ClientDTO> clientListByType = ClientLoader.getInstance().getClientDTOByType(type);
    output += "<option value=\"\">Select Client</option>";
    if (clientListByType != null && clientListByType.size() > 0) {
        int size = clientListByType.size();
        for (int i = 0; i < size; i++) {
            ClientDTO c_dto = (ClientDTO) clientListByType.get(i);
            output += "<option value=\"" + c_dto.getId() + "\">" + c_dto.getClient_id() + "</option>";
        }
    }
%>
<%=output%>