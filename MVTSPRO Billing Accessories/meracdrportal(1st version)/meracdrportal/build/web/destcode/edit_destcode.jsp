<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<html>
    <head>
        <title>24Billing :: Add DestCode</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <html:form action="/destcode/editDestCode" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Edit Destination Code</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="DestCodeForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Destination Code</th>
                                    <td valign="top" >
                                        <html:text property="destcode" /><br/>
                                        <html:messages id="destcode" property="destcode">
                                            <bean:write name="destcode"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Destination Name</th>
                                    <td valign="top" >
                                        <html:text property="destcode_name" /><br/>
                                        <html:messages id="destcode_name" property="destcode_name">
                                            <bean:write name="destcode_name"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>