<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.mvtscdr.MvtsCdrDTO,com.myapp.struts.mvtscdr.MvtsCdrDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    MvtsCdrDAO mvtsDao = new MvtsCdrDAO();
    String links = "";
    boolean disOrigin = true;
    boolean disTerm = true;

    String originId = (String) request.getParameter("oid");
    if (originId == null) {
        originId = "";
        disOrigin = true;
    } else {
        disOrigin = false;
        links += "&oid=" + originId;
    }
    
    String termId = (String) request.getParameter("tid");
    if (termId == null) {
        termId = "";
        disTerm = true;
    } else {
        disTerm = false;
        links += "&tid=" + termId;
    }

    String recordPerPage = (String) request.getParameter("recordPerPage");
    if (recordPerPage == null) {
        recordPerPage = "" + Constants.PER_PAGE_RECORD;
    }

    String pNo = (String) request.getParameter("pageNo");
    if (pNo == null) {
        pNo = "1";
    }


    String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
    if (msg == null) {
        msg = "";
    }

    ArrayList<Integer> days = Utils.getDay();
    ArrayList<String> months = Utils.getMonth();
    ArrayList<Integer> years = Utils.getYear();
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> minsec = Utils.getTimeValue(60);
    ArrayList<MvtsCdrDTO> mvtscdrList = (ArrayList<MvtsCdrDTO>) request.getSession(true).getAttribute("MvtsCdrDTO");
    MvtsCdrDTO searchDTO = (MvtsCdrDTO) request.getSession(true).getAttribute("SearchCdrDTO");
    NumberFormat formatter = new DecimalFormat("00");
    ArrayList<ClientDTO> originClientList = ClientLoader.getInstance().getClientDTOByType(Constants.ORIGINATION);
    ArrayList<ClientDTO> termClientList = ClientLoader.getInstance().getClientDTOByType(Constants.TERMINATION);
%>
<html>
    <head>
        <title>24Billing :: Mvts Cdr List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div class="top"></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right" style="width:80%">
                <div class="pad_10 border_left">
                    <html:form action="/mvtscdr/listMvtsCdr.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:700px;" border="0" cellpadding="0" cellspacing="0">
                                <tr><td colspan="2" class="center-align bold">Origination</td><td align="center" class="center-align bold" colspan="2">Termination</td></tr>
                                <tr>
                                    <th>Client ID</th>
                                    <td>
                                        <html:select property="originId" styleClass="joriginId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (originClientList != null && originClientList.size() > 0) {
                                                    int size = originClientList.size();
                                                     for (int i = 0; i < size; i++) {
                                                         ClientDTO c_dto = (ClientDTO) originClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Client ID</th>
                                    <td><html:select property="termId" value="<%=termId%>" styleClass="jtermId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (termClientList != null && termClientList.size() > 0) {
                                                    int size = termClientList.size();
                                                     for (int i = 0; i < size; i++) {
                                                         ClientDTO c_dto = (ClientDTO) termClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                        </td>
                                </tr>
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origination_ip" disabled="<%=disOrigin%>" styleClass="joriginIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                if (!originId.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(originId));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                    <th>Termination  IP</th>
                                    <td><html:select property="termination_ip" disabled="<%=disTerm%>" styleClass="jtermIp">
                                            <html:option value="0">All</html:option>
                                            <%
                                                if (!termId.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(termId));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                         GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                         if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                </tr>
                                <tr>
                                    <th>Destination</th>
                                    <td><html:text property="originDestination" styleClass="jorigindest" disabled="<%=disOrigin%>" /></td>
                                    <th>Destination</th>
                                    <td><html:text property="termDestination" styleClass="jtermdest" disabled="<%=disTerm%>" /></td>
                                </tr>
                                <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Year</div><div class="fl_left month">Month</div><div class="fl_left">Day</div><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                <tr>
                                    <th>From Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>To Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=recordPerPage%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pNo%>" />
                                    </td>
                                </tr>
                                <tr><th>Call Type</th><td colspan="3"><html:radio property="callType" value="1" /> Successful <html:radio property="callType" value="2" /> Failed</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>
                    <div class="over_flow_content">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <%
                                    int dataListSize = 0;
                                    int pageNo = 1;
                                    int prevPageNo = 1;
                                    int nextPageNo = 1;
                                    int totalPages = 1;
                                    int total_time = 0;
                                    dataListSize = mvtsDao.getTotalMvtsCDR(searchDTO);
                                    int perPageRecord = Integer.parseInt(recordPerPage);
                                    if (perPageRecord == 0) {
                                        perPageRecord = Constants.PER_PAGE_RECORD;
                                    }

                                    if (dataListSize > 0) {
                                        totalPages = dataListSize / perPageRecord;
                                        if (dataListSize % perPageRecord != 0) {
                                            totalPages++;
                                        }
                                    }

                                    if (request.getParameter("pageNo") != null) {
                                        pageNo = Integer.parseInt(request.getParameter("pageNo"));
                                    }
                                    int pageStart = (pageNo - 1) * perPageRecord;
                                    if (pageNo > 1) {
                                        prevPageNo = pageNo - 1;
                                    }
                                    nextPageNo = pageNo;
                                    if ((pageNo + 1) <= totalPages) {
                                        nextPageNo = pageNo + 1;
                                    }

                                    String firstLink = request.getContextPath() + "/mvtscdr/listMvtsCdr.do?pageNo=1&recordPerPage=" + perPageRecord + links;
                                    String prevLink = request.getContextPath() + "/mvtscdr/listMvtsCdr.do?pageNo=" + prevPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String nextLink = request.getContextPath() + "/mvtscdr/listMvtsCdr.do?pageNo=" + nextPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String lastLink = request.getContextPath() + "/mvtscdr/listMvtsCdr.do?pageNo=" + totalPages + "&recordPerPage=" + perPageRecord + links;
                                %>
                                <tr><td colspan="15" align="center"><input type="hidden" name="pageNo" value="<%=pageNo%>"/>Page&nbsp;<%=pageNo%>&nbsp;of&nbsp;<%=totalPages%></td></tr>
                                <tr class="header"><td colspan="15"  align="center"><a href="<%=firstLink%>">&laquo;First</a> | <a href="<%=prevLink%>">&laquo; Prev</a> | <a href="<%=nextLink%>">Next &raquo;</a> | <a href="<%=lastLink%>">Last &raquo;</a></td></tr>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <th colspan="5" width="30%">Origination</th>
                                    <th colspan="5" width="30%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    String bg_class = "odd";
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            MvtsCdrDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }

                                            total_time += obj.getElapsed_time();
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=pageStart + inc + 1%>.</td>
                                    <td><%=obj.getIn_dnis()%></td>
                                     <td><%=obj.getIn_ani()%></td>
                                    <td><%=obj.getOriginClient_id()%></td>
                                    <td><%=obj.getRemote_src_sig_address()%></td>
                                    <td><%=obj.getOriginClient_prefix()%></td>
                                    <td><%=obj.getOriginDest()%></td>
                                     <td><%=obj.getOut_ani()%></td>
                                    <td><%=obj.getTermClient_id()%></td>
                                    <td><%=obj.getRemote_dst_sig_address()%></td>
                                    <td><%=obj.getTermClient_prefix()%></td>
                                    <td><%=obj.getTermDest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getElapsed_time())%></td>
                                    <td><%=obj.getConnect_time()%></td>
                                    <td><%=obj.getPdd()%></td>
                                </tr>
                                <%   }%>
                                <tr><th colspan="12">Total: </th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td></tr>
                                <%   }else{ %>
                                <tr><th colspan="15">No Content Found!</th></tr>
                                <% }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>