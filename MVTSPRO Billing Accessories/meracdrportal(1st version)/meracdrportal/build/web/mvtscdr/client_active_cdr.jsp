<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.mvtscdr.MvtsCdrDTO,com.myapp.struts.mvtscdr.MvtsCdrDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    
    ArrayList<MvtsCdrDTO> mvtscdrList = (ArrayList<MvtsCdrDTO>) request.getSession(true).getAttribute("MvtsCdrDTO");
    MvtsCdrDTO searchDTO = (MvtsCdrDTO) request.getSession(true).getAttribute("SearchCdrDTO");
    NumberFormat formatter = new DecimalFormat("00");
%>
<html>
    <head>
        <title>24Billing :: Mvts Cdr List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div class="top"></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right" style="width:80%">
                <div class="pad_10 border_left">
                    <div>
                        <%
                           
                            String bg_class = "odd";
                            int total_time = 0;
                        %>
                        <%
                            switch (login_dto.getClientType()) {
                                case Constants.BOTH:%>
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <th colspan="4" width="30%">Origination</th>
                                    <th colspan="4" width="30%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (hh:mm:ss)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%

                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            MvtsCdrDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getElapsed_time();
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=obj.getIn_dnis()%></td>
                                    <td><%=obj.getOriginClient_id()%></td>
                                    <td><%=obj.getRemote_src_sig_address()%></td>
                                    <td><%=obj.getOriginClient_prefix()%></td>
                                    <td><%=obj.getOriginDest()%></td>
                                    <td><%=obj.getTermClient_id()%></td>
                                    <td><%=obj.getRemote_dst_sig_address()%></td>
                                    <td><%=obj.getTermClient_prefix()%></td>
                                    <td><%=obj.getTermDest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getElapsed_time())%></td>
                                    <td><%=obj.getConnect_time()%></td>
                                    <td><%=obj.getPdd()%></td>
                                </tr>
                                <%   }%>
                                <tr><th colspan="10">Total: </th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td></tr>
                                <%
                                } else {%>
                                <tr><th colspan="10">No Content Found!</th></tr>
                                <%    }
                                %>
                            </tbody>
                        </table>
                        <% break;
                            case Constants.ORIGINATION:%>
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <th colspan="5" width="30%">Origination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            MvtsCdrDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getElapsed_time();
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=obj.getIn_dnis()%></td>
                                    <td><%=obj.getIn_ani()%></td>
                                    <td><%=obj.getOriginClient_id()%></td>
                                    <td><%=obj.getRemote_src_sig_address()%></td>
                                    <td><%=obj.getOriginClient_prefix()%></td>
                                    <td><%=obj.getOriginDest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getElapsed_time())%></td>
                                    <td><%=obj.getConnect_time()%></td>
                                    <td><%=obj.getPdd()%></td>
                                </tr>
                                <%   }%>
                                <tr><th colspan="7">Total: </th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td></tr>
                                <%} else {%>
                                <tr><th colspan="10">No Content Found!</th></tr>
                                <%    }
                                %>
                            </tbody>
                        </table>
                        <%
                                break;
                            case Constants.TERMINATION:%>
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <th colspan="5" width="30%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                </tr>
                                <tr class="header">
                                    <th>Source No</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%

                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            MvtsCdrDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getElapsed_time();
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td><%=obj.getIn_dnis()%></td>
                                    <td><%=obj.getOut_ani()%></td>
                                    <td><%=obj.getTermClient_id()%></td>
                                    <td><%=obj.getRemote_dst_sig_address()%></td>
                                    <td><%=obj.getTermClient_prefix()%></td>
                                    <td><%=obj.getTermDest()%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getElapsed_time())%></td>
                                    <td><%=obj.getConnect_time()%></td>
                                    <td><%=obj.getPdd()%></td>
                                </tr>
                                <%   }%>
                                <tr><th colspan="7">Total: </th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td></tr>
                                <%} else {%>
                                <tr><th colspan="10">No Content Found!</th></tr>
                                <%    }
                                %>
                            </tbody>
                        </table>
                        <%
                                    break;
                            }%>

                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>