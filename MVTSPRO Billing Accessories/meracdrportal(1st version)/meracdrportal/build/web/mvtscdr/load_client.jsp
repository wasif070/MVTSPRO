<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO" %>
<%
    String searchVal = "";
    String output = "";
    int type = 0;
    if (request.getParameter("search") != null) {
        searchVal = request.getParameter("search");
    }
    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }

    ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOByType(type);
    output += "<ul class='width_160'>";
    if (clientList != null && clientList.size() > 0) {
        int size =clientList.size();
        for (int i = 0; i < size; i++) {
            ClientDTO c_dto = (ClientDTO) clientList.get(i);
            if (c_dto.getClient_id().startsWith(searchVal)) {
                output += "<li title=\"" + c_dto.getClient_id() + "\" rel=\"" + c_dto.getId() + "\">" + c_dto.getClient_id() + "</li>";
            }
        }
    }
    output += "</ul>";
%>
<%=output%>