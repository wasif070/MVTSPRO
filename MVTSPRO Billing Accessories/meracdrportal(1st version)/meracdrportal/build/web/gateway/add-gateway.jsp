<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOList();
%>
<html>
    <head>
        <title>24Billing :: Add Gateway</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <html:form action="/gateway/addGateway" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Add New Gateway IP</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="GatewayForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Gateway IP <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="gateway_ip" /><br/>
                                        <html:messages id="gateway_ip" property="gateway_ip">
                                            <bean:write name="gateway_ip"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Gateway Type <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:select property="gateway_type" styleId="jgateway_type">
                                            <html:option value="">Select Gateway Type</html:option>
                                            <%
                                                for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="gateway_type" property="gateway_type">
                                            <bean:write name="gateway_type"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Client</th>
                                    <td valign="top">
                                        <html:select property="clientId" styleId="jclientId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (clientList != null && clientList.size() > 0) {
                                                    int size = clientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) clientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>
                                            <%}
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Description</th>
                                    <td valign="top" >
                                        <html:text property="gateway_name" /><br/>
                                        <html:messages id="gateway_name" property="gateway_name">
                                            <bean:write name="gateway_name"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:select property="gateway_status">
                                            <%
                                                for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="gateway_status" property="gateway_status">
                                            <bean:write name="gateway_status"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>