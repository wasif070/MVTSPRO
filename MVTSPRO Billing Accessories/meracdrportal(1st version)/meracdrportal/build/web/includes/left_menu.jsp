<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants" %>
<% //LoginDTO login_dto = null;
    if (request.getSession(true).getAttribute(Constants.LOGIN_DTO) != null) {
        login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
    }
%>
<div class="pad_10">
    <ul id="browser" class="filetree treeview">
        <%
            if (login_dto.getSuperUser()) {
        %>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Clients</span>
            <ul>
                <li><span ><a href="../clients/listClient.do?list_all=1">Client List</a></span></li>
                <li><span ><a href="../clients/add-client.jsp" >Add New Client</a></span></li>
                <li class="last"><span ><a href="../clients/listRechargeClient.do?list_all=1" >Recharge Client</a></span></li>
            </ul>
        </li>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Gateway</span>
            <ul>
                <li><span ><a href="../gateway/listGateway.do?list_all=1">Gateway List</a></span></li>
                <li class="last"><span ><a href="../gateway/add-gateway.jsp" >Add New Gateway</a></span></li>
            </ul>
        </li>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Rate Plan</span>
            <ul>
                <li><span ><a href="../rateplan/listRateplan.do?list_all=1">Rate Plan List</a></span></li>
                <li class="last"><span ><a href="../rateplan/new_rateplan.jsp" >Add Rate Plan</a></span></li>
            </ul>
        </li>
        <!--
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Destination</span>
            <ul>
                <li><span ><a href="../destcode/listDestCode.do?list_all=1">Destination List</a></span></li>
                <li><span ><a href="../destcode/add_destcode.jsp" >Add Destination</a></span></li>
                <li><span ><a href="../destcode/upload_destcode.jsp" >Upload Destination</a></span></li>
                <li class="last"><span ><a href="../destcode/download.jsp">Download Destination</a></span></li>
            </ul>
        </li>
        -->
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Users</span>
            <ul>
                <li><span ><a href="../user/listUser.do?list_all=1">User List</a></span></li>
                <li class="last"><span ><a href="../user/add-user.jsp" >Add New User</a></span></li>
            </ul>
        </li>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Reports</span>
            <ul>
                <!--
                <li><span ><a href="../mvtscdr/activeMvtsCdr.do?list_all=1">Active Call</a></span></li>
                <li><span ><a href="../mvtscdr/listMvtsCdr.do?list_all=1">CDR Report</a></span></li>
                -->
                <li><span ><a href="../reports/listCdr.do?list_all=1">CDR Report</a></span></li>
                <li><span ><a href="../reports/qualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li><span ><a href="../transactions/listTransaction.do?list_all=1">Payment History</a></span></li>
                <li class="last"><span ><a href="../transactions/transSummery.do?list_all=1">Payment Summery</a></span></li>
            </ul>
        </li>
        <% } else {%>
        <li><span ><a href="../transactions/clientSummery.do">Home</a></span></li>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Reports</span>
            <ul>
                <li><span ><a href="../reports/listClientCdr.do?list_all=1">CDR Report</a></span></li>
                <li><span ><a href="../reports/cQualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li class="last"><span ><a href="../transactions/clientTrans.do?list_all=1">Payment Received</a></span></li>
            </ul>
        </li>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span >Clients</span>
            <ul>
                <li class="last"><span ><a href="../clients/getClientInfo.do">Edit Profile</a></span></li>
            </ul>
        </li>
        <li><span ><a href="../rates/clientRate.do">Rate Tariff</a></span></li>
        <% }%>
        <li class="last"><span ><a href="../login/logout.do">Logout</a></span></li>
    </ul>
</div>