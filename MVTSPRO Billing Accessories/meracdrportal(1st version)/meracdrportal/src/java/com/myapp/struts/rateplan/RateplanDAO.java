/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import com.mysql.jdbc.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class RateplanDAO {

    static Logger logger = Logger.getLogger(RateplanDAO.class.getName());

    public RateplanDAO() {
    }

    public ArrayList<RateplanDTO> getRateplanDTOsWithSearchParam(ArrayList<RateplanDTO> list, RateplanDTO udto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RateplanDTO dto = (RateplanDTO) i.next();
                if ((udto.searchWithName && !dto.getRateplan_name().toLowerCase().startsWith(udto.getRateplan_name()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<RateplanDTO> getRateplanDTOsSorted(ArrayList<RateplanDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RateplanDTO dto1 = (RateplanDTO) o1;
                    RateplanDTO dto2 = (RateplanDTO) o2;
                    if (dto1.getRateplan_id() < dto2.getRateplan_id()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addRateplanInformation(RateplanDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select rateplan_name from mvts_rateplan where rateplan_name=? and rateplan_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rateplan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into mvts_rateplan(rateplan_name,rateplan_des,rateplan_status,rateplan_create_date,user_id) values(?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getRateplan_name());
            ps.setString(2, p_dto.getRateplan_des());
            ps.setInt(3, p_dto.getRateplan_status());
            ps.setLong(4, System.currentTimeMillis());
            ps.setLong(5, p_dto.getUser_id());



            ps.executeUpdate();
            RateplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRateplanInformation(RateplanDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select rateplan_name from mvts_rateplan where rateplan_name = ? and rateplan_id!=" + p_dto.getRateplan_id() + " and rateplan_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rate Plan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update mvts_rateplan set rateplan_name=?,rateplan_des=?,rateplan_status=?  where rateplan_id=" + p_dto.getRateplan_id();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getRateplan_name());
            ps.setString(2, p_dto.getRateplan_des());
            ps.setInt(3, p_dto.getRateplan_status());
            //ps.setLong(4, p_dto.getUser_id());

            ps.executeUpdate();
            RateplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        String selectedIdsString = Utils.implodeArray(destcodeIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_rateplan set rateplan_delete = 1"
                    + " where rateplan_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                RateplanLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while deleting the Rate Plan!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
