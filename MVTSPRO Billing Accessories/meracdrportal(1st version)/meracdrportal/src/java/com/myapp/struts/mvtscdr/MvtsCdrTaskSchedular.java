package com.myapp.struts.mvtscdr;

import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class MvtsCdrTaskSchedular {

    public MvtsCdrTaskSchedular() {
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrDTOs(LoginDTO l_dto, int start, int end) {
        MvtsCdrDAO mvtsDAO = new MvtsCdrDAO();
        if (l_dto.getSuperUser()) {
            return mvtsDAO.getMvtsCdrDTOList(start, end);
        } else {
            return mvtsDAO.getMvtsCdrDTOByClient(l_dto.getId(), start, end);
        }
    }

    public ArrayList<MvtsCdrDTO> getActiveMvtsCdrDTOs(LoginDTO l_dto, int start, int end) {
        MvtsCdrDAO mvtsDAO = new MvtsCdrDAO();
        if (l_dto.getSuperUser()) {
            return mvtsDAO.getActiveMvtsCdrDTOList(start, end);
        } else {
            return mvtsDAO.getClientActiveMvtsCdrDTOList(l_dto.getId(), start, end);
        }
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrDTOsWithSearchParam(MvtsCdrDTO sdto, LoginDTO l_dto, int start, int end) {
        MvtsCdrDAO mvtsDAO = new MvtsCdrDAO();
        if (l_dto.getSuperUser()) {
            return mvtsDAO.getMvtsCdrDTOsWithSearchParam(sdto, start, end);
        } else {
            return mvtsDAO.getClientMvtsCdrDTOsWithSearchParam(l_dto.getId(), sdto, start, end);
        }
    }

    public ArrayList<MvtsQualityDTO> getSummaryMvtsCdrDTOs(MvtsCdrDTO sdto, LoginDTO l_dto) {
        MvtsCdrDAO mvtsDAO = new MvtsCdrDAO();
        if (l_dto.getSuperUser()) {
            return mvtsDAO.getSummaryMvtsCdrDTOList(sdto);
        } else {
            return mvtsDAO.getClientSummaryMvtsCdrDTOList(sdto, l_dto.getId());
        }
    }
}