package com.myapp.struts.destcode;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class DestCodeDAO {

    static Logger logger = Logger.getLogger(DestCodeDAO.class.getName());

    public DestCodeDAO() {
    }

    public ArrayList<DestCodeDTO> getDestCodeDTOsWithSearchParam(ArrayList<DestCodeDTO> list, DestCodeDTO udto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                DestCodeDTO dto = (DestCodeDTO) i.next();
                if ((udto.searchWithDestCode && !dto.getDestcode().toLowerCase().startsWith(udto.getDestcode()))
                        || (udto.searchWithDestName && !dto.getDestcode_name().toLowerCase().startsWith(udto.getDestcode_name()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<DestCodeDTO> getDestCodeDTOsSorted(ArrayList<DestCodeDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    DestCodeDTO dto1 = (DestCodeDTO) o1;
                    DestCodeDTO dto2 = (DestCodeDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addDestCodeInformation(DestCodeDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select destcode from destination_code where destcode=? and destcode_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getDestcode());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate DestCode.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into destination_code(destcode,destcode_name) values(?,?)";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDestcode());
            ps.setString(2, p_dto.getDestcode_name());


            ps.executeUpdate();
            DestCodeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding destcode: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editDestCodeInformation(DestCodeDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select destcode from destination_code where destcode = ? and id!=" + p_dto.getId() + " and destcode_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getDestcode());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User IP.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update destination_code set destcode=?,destcode_name=?  where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDestcode());
            ps.setString(2, p_dto.getDestcode_name());

            ps.executeUpdate();
            DestCodeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing destcode: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteDestCode(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update destination_code set destcode_delete = 1"
                    + " where id =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                DestCodeLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while editing destcode: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError uploadFlieDestCode(ArrayList<DestCodeDTO> dto_array) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        String sql = "";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            if (dto_array.size() > 0) {
                for (int inc = 0; inc < dto_array.size(); inc++) {
                    DestCodeDTO p_dto = dto_array.get(inc);
                    sql = "select destcode,id from destination_code where destcode = ? and destcode_delete=0";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getDestcode());
                    ResultSet resultSet = ps.executeQuery();
                    if (resultSet.next()) {
                        sql = "update destination_code set destcode=?,destcode_name=?  where id=" + resultSet.getInt("id");
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.setString(1, p_dto.getDestcode());
                        ps.setString(2, p_dto.getDestcode_name());
                        ps.executeUpdate();
                    } else {
                        sql = "insert into destination_code(destcode,destcode_name) values(?,?)";
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.setString(1, p_dto.getDestcode());
                        ps.setString(2, p_dto.getDestcode_name());
                        ps.executeUpdate();
                    }

                }
            }
            DestCodeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding destcode: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public StringBuffer getCSVStrings(DestCodeDTO udto) {
        StringBuffer csvString = new StringBuffer();
        ArrayList<DestCodeDTO> destCodeList = DestCodeLoader.getInstance().getDestCodeDTOList();
        if (destCodeList.size() > 0) {
            for (int inc = 0; inc < destCodeList.size(); inc++) {
                DestCodeDTO dto = destCodeList.get(inc);
                if (dto.getDestcode().toLowerCase().startsWith(udto.getDestcode())) {
                    csvString.append(dto.getDestcode());
                    csvString.append(',');
                    csvString.append(dto.getDestcode_name());
                    csvString.append('\n');
                }
            }
        }
        return csvString;
    }
    
    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        String selectedIdsString = Utils.implodeArray(destcodeIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update destination_code set destcode_delete = 1"
                    + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                DestCodeLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while deleting the destination code!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
