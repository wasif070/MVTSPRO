package com.myapp.struts.mvtscdr;

/**
 *
 * @author Administrator
 */
public class MvtsQualityDTO {
    private long period_start;
    private long period_end;
    
    private long originId;
    private String originClient;
    private String origination_ip;
    private String origin_dest;
    
    private long termId;
    private String termClient;
    private String termination_ip;
    private String term_dest;
    
    private int duration;
    private long asr;
    private int acd;
    private double avg_pdd;
    
    private int total_success;
    private int total_fail;

    public MvtsQualityDTO() {
    }

    public int getAcd() {
        return acd;
    }

    public void setAcd(int acd) {
        this.acd = acd;
    }

    public long getAsr() {
        return asr;
    }
    
    public void setAsr(long asr) {
        this.asr = asr;
    }

    public double getAvg_pdd() {
        return avg_pdd;
    }

    public void setAvg_pdd(double avg_pdd) {
        this.avg_pdd = avg_pdd;
    }

    public long getPeriod_end() {
        return period_end;
    }

    public void setPeriod_end(long period_end) {
        this.period_end = period_end;
    }

    public long getPeriod_start() {
        return period_start;
    }

    public void setPeriod_start(long period_start) {
        this.period_start = period_start;
    }
    
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getOriginClient() {
        return originClient;
    }

    public void setOriginClient(String originClient) {
        this.originClient = originClient;
    }

    public long getOriginId() {
        return originId;
    }

    public void setOriginId(long originId) {
        this.originId = originId;
    }

    public String getOrigin_dest() {
        return origin_dest;
    }

    public void setOrigin_dest(String origin_dest) {
        this.origin_dest = origin_dest;
    }

    public String getOrigination_ip() {
        return origination_ip;
    }

    public void setOrigination_ip(String origination_ip) {
        this.origination_ip = origination_ip;
    }

    public String getTermClient() {
        return termClient;
    }

    public void setTermClient(String termClient) {
        this.termClient = termClient;
    }

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public String getTerm_dest() {
        return term_dest;
    }

    public void setTerm_dest(String term_dest) {
        this.term_dest = term_dest;
    }

    public String getTermination_ip() {
        return termination_ip;
    }

    public void setTermination_ip(String termination_ip) {
        this.termination_ip = termination_ip;
    }

    public int getTotal_fail() {
        return total_fail;
    }

    public void setTotal_fail(int total_fail) {
        this.total_fail = total_fail;
    }

    public int getTotal_success() {
        return total_success;
    }

    public void setTotal_success(int total_success) {
        this.total_success = total_success;
    }
    
}
