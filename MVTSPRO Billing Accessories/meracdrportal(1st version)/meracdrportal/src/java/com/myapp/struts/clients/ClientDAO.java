package com.myapp.struts.clients;

import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.myapp.struts.transactions.TransactionLoader;
import com.myapp.struts.user.UserLoader;

public class ClientDAO {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ClientDAO() {
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ArrayList<ClientDTO> list, ClientDTO udto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((udto.searchWithClientID && !dto.getClient_id().toLowerCase().startsWith(udto.getClient_id()))
                        || (udto.searchWithStatus && dto.getClient_status() != udto.getClient_status())
                        || (udto.searchWithType && dto.getClient_type() != udto.getClient_type())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addClientInformation(long user_id, ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select client_id from clients where client_id=? and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            if (p_dto.getIncoming_prefix() != null && p_dto.getIncoming_prefix().length() > 0) {
                sql = "select incoming_prefix from incoming_prefix where incoming_prefix=?";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getIncoming_prefix());
                resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Incoming Prefix.");
                    resultSet.close();
                    return error;
                }
                resultSet.close();
            }

            if (p_dto.getOutgoing_prefix() != null && p_dto.getOutgoing_prefix().length() > 0) {
                sql = "select outgoing_prefix from outgoing_prefix where outgoing_prefix=?";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getOutgoing_prefix());
                resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Outgoing Prefix.");
                    resultSet.close();
                    return error;
                }
                resultSet.close();
            }

            sql = "insert into clients(client_id,client_password,client_name,client_email,client_type,client_status,rateplan_id,client_credit_limit,client_balance) values(?,?,?,?,?,?,?,?,?);";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            ps.setInt(5, p_dto.getClient_type());
            ps.setInt(6, p_dto.getClient_status());
            ps.setLong(7, p_dto.getRateplan_id());
            ps.setDouble(8, p_dto.getClient_credit_limit());
            ps.setDouble(9, p_dto.getClient_balance());

            ps.executeUpdate();

            int last_id = 0;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                last_id = rs.getInt(1);
                sql = "insert into incoming_prefix(client_id,incoming_prefix,incoming_to) values(?,?,?)";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setInt(1, last_id);
                ps.setString(2, p_dto.getIncoming_prefix());
                ps.setString(3, p_dto.getIncoming_to());
                ps.executeUpdate();

                sql = "insert into outgoing_prefix(client_id,outgoing_prefix,outgoing_to) values(?,?,?)";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setInt(1, last_id);
                ps.setString(2, p_dto.getOutgoing_prefix());
                ps.setString(3, p_dto.getOutgoing_to());
                ps.executeUpdate();
                if (p_dto.getClient_balance() > 0) {
                    sql = "insert into client_transactions(client_id,transaction_recharge,transaction_des,transaction_date,user_id) values(?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, last_id);
                    ps.setDouble(2, p_dto.getClient_balance());
                    ps.setString(3, "Initial Balance");
                    ps.setLong(4, System.currentTimeMillis());
                    ps.setLong(5, user_id);
                    ps.executeUpdate();
                }

            }
            rs.close();
            ClientLoader.getInstance().forceReload();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError clientEditInformation(ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "update clients set client_id=?,client_password=?,client_name=?,client_email=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            // ps.setDouble(5, p_dto.getClient_credit_limit());
            ps.executeUpdate();

            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editClientInformation(ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select client_id from clients where client_id = ? and id!=" + p_dto.getId() + " and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            if (p_dto.getIncoming_prefix().length() > 0) {
                sql = "select incoming_prefix from incoming_prefix where incoming_prefix=? and client_id!=" + p_dto.getId();
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getIncoming_prefix());
                resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Incoming Prefix.");
                    resultSet.close();
                    return error;
                }
                resultSet.close();
            }

            if (p_dto.getOutgoing_prefix().length() > 0) {
                sql = "select outgoing_prefix from outgoing_prefix where outgoing_prefix=? and client_id!=" + p_dto.getId();
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getOutgoing_prefix());
                resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Outgoing Prefix.");
                    resultSet.close();
                    return error;
                }
                resultSet.close();
            }

            sql = "update clients set client_id=?,client_password=?,client_name=?,client_email=?,client_type=?,client_status=?,rateplan_id=?,client_credit_limit=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            logger.debug("sqL:::" + sql);
            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            ps.setInt(5, p_dto.getClient_type());
            ps.setInt(6, p_dto.getClient_status());
            ps.setLong(7, p_dto.getRateplan_id());
            ps.setDouble(8, p_dto.getClient_credit_limit());
            ps.executeUpdate();

            sql = "update incoming_prefix set incoming_prefix =?,incoming_to =? where client_id= " + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getIncoming_prefix());
            ps.setString(2, p_dto.getIncoming_to());
            if (ps.executeUpdate() == 0) {
                sql = "insert into incoming_prefix(client_id,incoming_prefix,incoming_to) values(?,?,?)";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setLong(1, p_dto.getId());
                ps.setString(2, p_dto.getIncoming_prefix());
                ps.setString(3, p_dto.getIncoming_to());
                ps.executeUpdate();
            }

            sql = "update outgoing_prefix set outgoing_prefix =?,outgoing_to =? where client_id= " + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getOutgoing_prefix());
            ps.setString(2, p_dto.getOutgoing_to());
            if (ps.executeUpdate() == 0) {
                sql = "insert into outgoing_prefix(client_id,outgoing_prefix,outgoing_to) values(?,?,?)";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setLong(1, p_dto.getId());
                ps.setString(2, p_dto.getOutgoing_prefix());
                ps.setString(3, p_dto.getOutgoing_to());
                ps.executeUpdate();
            }


            ClientLoader.getInstance().forceReload();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteClient(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        int i = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update clients set client_delete = 1"
                    + " where id =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ClientLoader.getInstance().forceReload();
            }
            sql = "update gateway set gateway_delete = 1"
                    + " where clientId =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "select gateway_name from gateway"
                    + " where clientId=" + cid;
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'" + dto.getGateway_name() + "'";
                logger.debug("multipleNameList:" + multipleNameList[i]);
                i++;
            }
            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_gateway set enable = 0" 
                    + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not block. Please try again.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientStatusUpdate(long clientIds[], int client_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        int i = 0;
        int sts = 0;
        int mvts_sts = 0;

        String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update clients set client_status = " + client_status
                    + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ClientLoader.getInstance().forceReload();
            }
            if(client_status == 0){
                sts = 0;
            }else{
                sts = 1;
            }
                        
            sql = "update gateway set gateway_status = " + sts
                    + " where clientId in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "select gateway_name from gateway"
                    + " where clientId in(" + selectedIdsString + ")";
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'" + dto.getGateway_name() + "'";
                logger.debug("multipleNameList:" + multipleNameList[i]);
                i++;
            }
            
            if (client_status == 0) {
                mvts_sts = 1;
            } else {
                mvts_sts = 0;
            }

            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_gateway set enable = " + mvts_sts
                    + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not block. Please try again.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientDelete(long clientIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        remotedbconnector.DBConnection dbConn = null;
        int i = 0;


        String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update clients set client_delete = 1"
                    + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ClientLoader.getInstance().forceReload();
            }
            
            sql = "update gateway set gateway_delete = 1"
                    + " where clientId in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }
            
            sql = "select gateway_name from gateway"
                    + " where clientId in(" + selectedIdsString + ")";
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'" + dto.getGateway_name() + "'";
                logger.debug("multipleNameList:" + multipleNameList[i]);
                i++;
            }

            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            sql = "update mvts_gateway set enable = 0"  
                    + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not delete. Please try again.");
            }
        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientRecharge(long user_id, long clientIds[], double amount[], String des[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        //String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            for (int inc = 0; inc < clientIds.length; inc++) {
                sql = "update clients set client_balance = (client_balance+" + amount[inc]
                        + ") where id = " + clientIds[inc];
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    sql = "insert into client_transactions(client_id,transaction_type,transaction_recharge,transaction_des,transaction_date,user_id) values(?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, clientIds[inc]);
                    ps.setInt(2, Constants.RECHARGE_AMOUNT);
                    ps.setDouble(3, amount[inc]);
                    ps.setString(4, des[inc]);
                    ps.setLong(5, System.currentTimeMillis());
                    ps.setLong(6, user_id);
                    ps.executeUpdate();
                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientReturn(long user_id, long clientIds[], double amount[], String des[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        //String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            for (int inc = 0; inc < clientIds.length; inc++) {
                sql = "update clients set client_balance = (client_balance-" + amount[inc]
                        + ") where id = " + clientIds[inc] + " and (client_balance-" + amount[inc] + ") >=0";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    sql = "insert into client_transactions(client_id,transaction_type,transaction_return,transaction_des,transaction_date,user_id) values(?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, clientIds[inc]);
                    ps.setInt(2, Constants.RETURN_AMOUNT);
                    ps.setDouble(3, amount[inc]);
                    ps.setString(4, des[inc]);
                    ps.setLong(5, System.currentTimeMillis());
                    ps.setLong(6, user_id);
                    ps.executeUpdate();
                } else {
                    error.setErrorType(2);
                    error.setErrorMessage("Your available balance is less than the return amount");
                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientReceive(long user_id, long clientIds[], double amount[], String des[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        //String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            for (int inc = 0; inc < clientIds.length; inc++) {
//                sql = "update clients set client_balance = (client_balance-" + amount[inc]
//                        + ") where id = " + clientIds[inc] + " and (client_balance-" + amount[inc] + ") >=0";
//                ps = dbConnection.connection.prepareStatement(sql);
//                if (ps.executeUpdate() > 0) {
                sql = "insert into client_transactions(client_id,transaction_type,transaction_receive,transaction_des,transaction_date,user_id) values(?,?,?,?,?,?)";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setLong(1, clientIds[inc]);
                ps.setInt(2, Constants.RECEIVE_AMOUNT);
                ps.setDouble(3, amount[inc]);
                ps.setString(4, des[inc]);
                ps.setLong(5, System.currentTimeMillis());
                ps.setLong(6, user_id);
                ps.executeUpdate();
//                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}