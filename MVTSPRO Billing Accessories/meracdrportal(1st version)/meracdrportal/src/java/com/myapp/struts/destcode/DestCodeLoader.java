package com.myapp.struts.destcode;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class DestCodeLoader {

    static Logger logger = Logger.getLogger(DestCodeLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<DestCodeDTO> destcodeList = null;
    private HashMap<Integer, DestCodeDTO> destcodeDTOByID = null;
    static DestCodeLoader destcodeLoader = null;

    public DestCodeLoader() {
        forceReload();
    }

    public static DestCodeLoader getInstance() {
        if (destcodeLoader == null) {
            createGatewayLoader();
        }
        return destcodeLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (destcodeLoader == null) {
            destcodeLoader = new DestCodeLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            destcodeList = new ArrayList<DestCodeDTO>();
            destcodeDTOByID = new HashMap<Integer, DestCodeDTO>();

            String sql = "select * from destination_code where destcode_delete=0";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                DestCodeDTO dto = new DestCodeDTO();
                dto.setId(resultSet.getInt("id"));
                dto.setDestcode(resultSet.getString("destcode"));
                dto.setDestcode_name(resultSet.getString("destcode_name"));
                dto.setSuperUser(false);
                destcodeDTOByID.put(dto.getId(), dto);
                destcodeList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in DestCodeLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<DestCodeDTO> getDestCodeDTOList() {
        checkForReload();
        return destcodeList;
    }

    public synchronized DestCodeDTO getDestCodeDTOByID(int id) {
        checkForReload();
        return destcodeDTOByID.get(id);
    }

    public synchronized DestCodeDTO getDestCodeForDialNumber(String dialNo) {
        checkForReload();
        DestCodeDTO dcDto = new DestCodeDTO();
        int prev = 0;
        for (int inc = 0; inc < destcodeList.size(); inc++) {
            DestCodeDTO sDto = destcodeList.get(inc);
            if (dialNo.startsWith(sDto.getDestcode())) {
                if (prev < sDto.getDestcode().length()) {
                    dcDto = destcodeList.get(inc);
                    prev = sDto.getDestcode().length();
                }
            }
        }
        return dcDto;
    }
}
