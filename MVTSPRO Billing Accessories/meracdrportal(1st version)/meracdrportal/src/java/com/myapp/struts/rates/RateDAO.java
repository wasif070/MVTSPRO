package com.myapp.struts.rates;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import com.mysql.jdbc.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class RateDAO {

    static Logger logger = Logger.getLogger(RateDAO.class.getName());

    public RateDAO() {
    }

    public ArrayList<RateDTO> getRateDTOsWithSearchParam(ArrayList<RateDTO> list, RateDTO rdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RateDTO dto = (RateDTO) i.next();
                if ((rdto.searchWithPrefixName && !dto.getRate_destination_code().toLowerCase().startsWith(rdto.getRate_destination_code()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<RateDTO> getRateDTOsSorted(ArrayList<RateDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RateDTO dto1 = (RateDTO) o1;
                    RateDTO dto2 = (RateDTO) o2;
                    if (dto1.getRate_id() < dto2.getRate_id()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addRateInformation(RateDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            long rate_id = 0;
            sql = "select rate_id from mvts_rates where rate_destination_code=? and rate_destination_name=? and rateplan_id=? and rate_per_min=? and rate_first_pulse=? and rate_next_pulse=?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRate_destination_code());
            ps.setString(2, p_dto.getRate_destination_name());
            ps.setLong(3, p_dto.getRateplan_id());
            ps.setDouble(4, p_dto.getRate_per_min());
            ps.setInt(5, p_dto.getRate_first_pulse());
            ps.setInt(6, p_dto.getRate_next_pulse());

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                rate_id = resultSet.getLong("rate_id");
            } else {
                sql = "select MAX(rate_id)+1 as rate_id from mvts_rates";
                ps = dbConnection.connection.prepareStatement(sql);
                resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    rate_id = resultSet.getLong("rate_id");
                }
            }
            resultSet.close();
            if (p_dto.getRate_day() != null && p_dto.getRate_day().length > 0) {
                for (int inc = 0; inc < p_dto.getRate_day().length; inc++) {
                    sql = "insert into mvts_rates(rate_id,rateplan_id,rate_destination_code,rate_destination_name,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_created_date,rate_status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, rate_id);
                    ps.setLong(2, p_dto.getRateplan_id());
                    ps.setString(3, p_dto.getRate_destination_code());
                    ps.setString(4, p_dto.getRate_destination_name());
                    ps.setFloat(5, p_dto.getRate_per_min());
                    ps.setInt(6, p_dto.getRate_first_pulse());
                    ps.setInt(7, p_dto.getRate_next_pulse());
                    ps.setInt(8, p_dto.getRate_grace_period());
                    ps.setInt(9, p_dto.getRate_failed_period());
                    ps.setString(10, p_dto.getRate_day()[inc]);
                    ps.setString(11, p_dto.getRate_fromhour()[inc]);
                    ps.setString(12, p_dto.getRate_frommin()[inc]);
                    ps.setString(13, p_dto.getRate_tohour()[inc]);
                    ps.setString(14, p_dto.getRate_tomin()[inc]);
                    ps.setLong(15, System.currentTimeMillis());
                    ps.setInt(16, p_dto.getRate_status());
                    ps.executeUpdate();
                }
            }

            RateLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRateInformation(RateDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            if (p_dto.getRate_day() != null && p_dto.getRate_day().length > 0) {
                for (int inc = 0; inc < p_dto.getRate_day().length; inc++) {
                    sql = "update mvts_rates set rate_destination_code=?,rate_destination_name=?,rate_per_min=?,rate_first_pulse=?,rate_next_pulse=?,rate_grace_period=?,rate_failed_period=?,rate_day=?,rate_from_hour=?,rate_from_min=?,rate_to_hour=?,rate_to_min=?,rate_status=?  where id=" + p_dto.getIds()[inc];
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getRate_destination_code());
                    ps.setString(2, p_dto.getRate_destination_name());
                    ps.setFloat(3, p_dto.getRate_per_min());
                    ps.setInt(4, p_dto.getRate_first_pulse());
                    ps.setInt(5, p_dto.getRate_next_pulse());
                    ps.setInt(6, p_dto.getRate_grace_period());
                    ps.setInt(7, p_dto.getRate_failed_period());
                    ps.setString(8, p_dto.getRate_day()[inc]);
                    ps.setString(9, p_dto.getRate_fromhour()[inc]);
                    ps.setString(10, p_dto.getRate_frommin()[inc]);
                    ps.setString(11, p_dto.getRate_tohour()[inc]);
                    ps.setString(12, p_dto.getRate_tomin()[inc]);
                    ps.setInt(13, p_dto.getRate_status());
                    ps.executeUpdate();
                }

            }
            RateLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;

        String selectedIdsString = Utils.implodeArray(destcodeIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_rates set rate_delete = 1"
                    + " where rate_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                RateLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while deleting the Rate Plan!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public StringBuffer getRateCSVStrings(RateDTO rdto) {
        StringBuffer csvString = new StringBuffer();
        ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOAllByPlan(rdto.getRateplan_id());
        if (rateList.size() > 0) {
            for (int inc = 0; inc < rateList.size(); inc++) {
                RateDTO dto = rateList.get(inc);
                csvString.append(dto.getRate_destination_code());
                csvString.append(',');
                csvString.append(dto.getRate_destination_name());
                csvString.append(',');
                csvString.append(dto.getRate_per_min());
                csvString.append(',');
                csvString.append(dto.getRate_first_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_next_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_grace_period());
                csvString.append(',');
                csvString.append(dto.getRate_failed_period());
                csvString.append(',');
                csvString.append(dto.getRate_sin_day());
                csvString.append(',');
                csvString.append(dto.getRate_sin_fromhour());
                csvString.append(',');
                csvString.append(dto.getRate_sin_frommin());
                csvString.append(',');
                csvString.append(dto.getRate_sin_tohour());
                csvString.append(',');
                csvString.append(dto.getRate_sin_tomin());
                csvString.append('\n');
            }
        }
        return csvString;
    }

    public MyAppError uploadFlieData(ArrayList<RateDTO> dto_array) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        String sql = "";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            if (dto_array.size() > 0) {
                for (int inc = 0; inc < dto_array.size(); inc++) {
                    RateDTO p_dto = dto_array.get(inc);
                    long rate_id = 0;
                    sql = "select rate_id from mvts_rates where rate_destination_code=? and rate_destination_name=? and rateplan_id=? and rate_per_min=? and rate_first_pulse=? and rate_next_pulse=?";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getRate_destination_code());
                    ps.setString(2, p_dto.getRate_destination_name());
                    ps.setLong(3, p_dto.getRateplan_id());
                    ps.setDouble(4, p_dto.getRate_per_min());
                    ps.setInt(5, p_dto.getRate_first_pulse());
                    ps.setInt(6, p_dto.getRate_next_pulse());

                    ResultSet resultSet = ps.executeQuery();
                    if (resultSet.next()) {
                        rate_id = resultSet.getLong("rate_id");
                    } else {
                        sql = "select MAX(rate_id)+1 as rate_id from mvts_rates";
                        ps = dbConnection.connection.prepareStatement(sql);
                        resultSet = ps.executeQuery();
                        if (resultSet.next()) {
                            rate_id = resultSet.getLong("rate_id");
                        }
                    }
                    resultSet.close();

                    sql = "insert into mvts_rates(rate_id,rateplan_id,rate_destination_code,rate_destination_name,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_created_date,rate_status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, rate_id);
                    ps.setLong(2, p_dto.getRateplan_id());
                    ps.setString(3, p_dto.getRate_destination_code());
                    ps.setString(4, p_dto.getRate_destination_name());
                    ps.setFloat(5, p_dto.getRate_per_min());
                    ps.setInt(6, p_dto.getRate_first_pulse());
                    ps.setInt(7, p_dto.getRate_next_pulse());
                    ps.setInt(8, p_dto.getRate_grace_period());
                    ps.setInt(9, p_dto.getRate_failed_period());
                    ps.setInt(10, p_dto.getRate_sin_day());
                    ps.setInt(11, p_dto.getRate_sin_fromhour());
                    ps.setInt(12, p_dto.getRate_sin_frommin());
                    ps.setInt(13, p_dto.getRate_sin_tohour());
                    ps.setInt(14, p_dto.getRate_sin_tomin());
                    ps.setLong(15, System.currentTimeMillis());
                    ps.setInt(16, p_dto.getRate_status());
                    ps.executeUpdate();
                }
            }
            RateLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding destcode: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
