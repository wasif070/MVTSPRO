package com.myapp.struts.clients;

import com.myapp.struts.rateplan.RateplanDTO;
import com.myapp.struts.rateplan.RateplanLoader;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ClientLoader {
    
    static Logger logger = Logger.getLogger(ClientLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<ClientDTO> clientList = null;
    private HashMap<Long, ClientDTO> clientDTOByID = null;
    private HashMap<Integer, IncomingPrefixDTO> incomingPrefixDTO = null;
    private HashMap<Integer, OutgoingPrefixDTO> outgoingPrefixDTO = null;
    static ClientLoader clientLoder = null;
    
    public ClientLoader() {
        forceReload();
    }
    
    public static ClientLoader getInstance() {
        if (clientLoder == null) {
            createClientLoader();
        }
        return clientLoder;
    }
    
    private synchronized static void createClientLoader() {
        if (clientLoder == null) {
            clientLoder = new ClientLoader();
        }
    }
    
    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            clientList = new ArrayList<ClientDTO>();
            clientDTOByID = new HashMap<Long, ClientDTO>();
            incomingPrefixDTO = new HashMap<Integer, IncomingPrefixDTO>();
            outgoingPrefixDTO = new HashMap<Integer, OutgoingPrefixDTO>();
            
            String sql = "select * from incoming_prefix";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                IncomingPrefixDTO inPrefixDto = new IncomingPrefixDTO();
                inPrefixDto.setId(resultSet.getInt("id"));
                inPrefixDto.setClient_id(resultSet.getInt("client_id"));
                inPrefixDto.setIncoming_prefix(resultSet.getString("incoming_prefix"));
                inPrefixDto.setIncoming_to(resultSet.getString("incoming_to"));
                incomingPrefixDTO.put(resultSet.getInt("client_id"), inPrefixDto);
            }
            
            sql = "select * from outgoing_prefix";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                OutgoingPrefixDTO outPrefixDto = new OutgoingPrefixDTO();
                outPrefixDto.setId(resultSet.getInt("id"));
                outPrefixDto.setClient_id(resultSet.getInt("client_id"));
                outPrefixDto.setOutgoing_prefix(resultSet.getString("outgoing_prefix"));
                outPrefixDto.setOutgoing_to(resultSet.getString("outgoing_to"));
                outgoingPrefixDTO.put(resultSet.getInt("client_id"), outPrefixDto);
            }
            
            sql = "select c.id,c.client_id,c.client_password,c.client_name,c.client_email,c.client_type,c.client_status,c.rateplan_id,c.client_credit_limit,c.client_balance from clients as c where client_delete=0";
            resultSet = statement.executeQuery(sql);
            ClientDTO dto = new ClientDTO();
            while (resultSet.next()) {
                dto = new ClientDTO();
                IncomingPrefixDTO inDto = new IncomingPrefixDTO();
                inDto = incomingPrefixDTO.get(resultSet.getInt("id"));
                
                OutgoingPrefixDTO outDto = new OutgoingPrefixDTO();
                outDto = outgoingPrefixDTO.get(resultSet.getInt("id"));
                
                dto.setId(resultSet.getLong("id"));
                dto.setClient_id(resultSet.getString("client_id"));
                dto.setClient_password(resultSet.getString("client_password"));
                dto.setClient_name(resultSet.getString("client_name"));
                dto.setClient_email(resultSet.getString("client_email"));
                
                if (inDto != null) {
                    dto.setIncoming_prefix(inDto.getIncoming_prefix());
                    dto.setIncoming_to(inDto.getIncoming_to());
                }
                
                if (outDto != null) {
                    dto.setOutgoing_prefix(outDto.getOutgoing_prefix());
                    dto.setOutgoing_to(outDto.getOutgoing_to());
                }
                
                dto.setClient_type(resultSet.getInt("client_type"));
                dto.setClient_type_name(Constants.CLIENT_TYPE_NAME[dto.getClient_type()]);
                dto.setClient_status(resultSet.getInt("client_status"));
                dto.setClient_status_name(Constants.LIVE_STATUS_STRING[dto.getClient_status()]);
                dto.setRateplan_id(resultSet.getLong("rateplan_id"));
                if (dto.getRateplan_id() > 0) {
                    RateplanDTO rdto = RateplanLoader.getInstance().getRateplanDTOByID(dto.getRateplan_id());
                    if (rdto != null) {
                        dto.setRateplan_name(rdto.getRateplan_name());
                    }
                }
                dto.setClient_credit_limit(resultSet.getDouble("client_credit_limit"));
                dto.setClient_balance(resultSet.getDouble("client_balance"));
                dto.setSuperUser(false);
                clientDTOByID.put(dto.getId(), dto);
                clientList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
    
    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }
    
    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }
    
    public synchronized ArrayList<ClientDTO> getClientDTOList() {
        checkForReload();
        return clientList;
    }
    
    public synchronized ClientDTO getClientDTOByID(long id) {
        checkForReload();
        return clientDTOByID.get(id);
    }
    
    public synchronized ArrayList<ClientDTO> getClientDTOByType(int type) {
        checkForReload();
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH) {
                clientListByType.add(dto);
            }
        }
        return clientListByType;
    }
}
