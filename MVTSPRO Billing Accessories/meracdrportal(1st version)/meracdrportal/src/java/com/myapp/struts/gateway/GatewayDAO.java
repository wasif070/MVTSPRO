package com.myapp.struts.gateway;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class GatewayDAO {

    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());

    public GatewayDAO() {
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(ArrayList<GatewayDTO> list, GatewayDTO udto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                GatewayDTO dto = (GatewayDTO) i.next();
                if ((udto.searchWithGatewayIP && !dto.getGateway_ip().toLowerCase().startsWith(udto.getGateway_ip()))
                        || (udto.searchWithStatus && dto.getGateway_status() != udto.getGateway_status())
                        || (udto.searchWithType && dto.getGateway_type() != udto.getGateway_type())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted(ArrayList<GatewayDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    GatewayDTO dto1 = (GatewayDTO) o1;
                    GatewayDTO dto2 = (GatewayDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addGatewayInformation(GatewayDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select gateway_name from gateway where gateway_name=? and gateway_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            //ps.setString(1, p_dto.getGateway_ip());
            ps.setString(1, p_dto.getGateway_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Gateway");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into gateway(gateway_ip,gateway_name,gateway_type,gateway_status,clientId) values(?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getGateway_ip());
            ps.setString(2, p_dto.getGateway_name());
            ps.setInt(3, p_dto.getGateway_type());
            ps.setInt(4, p_dto.getGateway_status());
            ps.setInt(5, p_dto.getClientId());
            

            ps.executeUpdate();
            GatewayLoader.getInstance().forceReload();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding gateway: ", ex);
        }
        try {
            String prefixes = "";
            if (p_dto.getClientId() != 0) {
                com.myapp.struts.clients.ClientDTO clientDTO = com.myapp.struts.clients.ClientLoader.getInstance().getClientDTOByID(p_dto.getClientId());
                ArrayList<com.myapp.struts.rates.RateDTO> rates = com.myapp.struts.rates.RateLoader.getInstance().getRateDTOList();                
                for (com.myapp.struts.rates.RateDTO rateDTO : rates) {
                    if (rateDTO.getRateplan_id() == clientDTO.getRateplan_id()) {
                        prefixes += rateDTO.getRate_destination_code() + "[0-9]*;";
                    }
                }
            }else{
                prefixes = "";
            }
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            if (p_dto.getGateway_type() == 0) {
                sql = "insert into mvts_gateway(gateway_name,description,equipment_type,enable,src_enable,dst_enable,reg_type,protocol,src_zone,src_address_list,dst_port_h323,dst_port_sip,src_codec_allow,dst_proxy_policy,dst_codecs_sort,src_dnis_prefix_allow,src_rbt_enable,dst_h323_first_answer_timeout,dst_sip_first_answer_timeout,dst_connect_msg_timeout,src_can_update_media_channel,src_faststart,src_response_faststart,src_tunneling,src_start_h245_after,dst_faststart,dst_tunneling,dst_start_h245_after,sip_gate_query,src_debug,dst_debug,radius_auth_call_enable,radius_acct_enable,stat_enable,disallow_dynamic_payload_type,src_drop_call_on_alerting_timeout,dst_default_protocol,src_codec_policy,dst_codec_policy,src_start_h245_can_be_forced,dst_start_h245_can_be_forced,dst_add_end_of_pulsing,src_h323_dtmf_policy,sip_reason_policy,radius_force_originate_telephony,always_use_ts_conf_id,dst_sip_router_timeout) ";
                sql += "value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Origination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 0);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, 1720);
                ps.setInt(12, 5060);
                ps.setInt(13, 1);
                ps.setInt(14, 5);
                ps.setInt(15, 1);
                ps.setString(16, prefixes);
                ps.setInt(17, 0);
                ps.setInt(18, 10000);
                ps.setInt(19, 32000);
                ps.setInt(20, 90);
                ps.setInt(21, 0);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, 0);
                ps.setInt(30, 1);
                ps.setInt(31, 0);
                ps.setInt(32, 0);
                ps.setInt(33, 0);
                ps.setInt(34, 1);
                ps.setInt(35, 0);
                ps.setInt(36, 0);
                ps.setInt(37, 1);
                ps.setInt(38, 1);
                ps.setInt(39, 1);
                ps.setInt(40, 1);
                ps.setInt(41, 1);
                ps.setInt(42, 1);
                ps.setInt(43, 0);
                ps.setInt(44, 0);
                ps.setInt(45, 0);
                ps.setInt(46, 0);
                ps.setInt(47, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not added. Please try again.");
                }
            }



            if (p_dto.getGateway_type() == 1) {
                sql = "insert into mvts_gateway(gateway_name,description,equipment_type,enable,src_enable,dst_enable,reg_type,protocol,dst_zone,dst_address,dst_port_h323,dst_port_sip,dst_proxy_policy,dst_codec_allow,dst_codecs_sort,dst_h323_first_answer_timeout,dst_sip_first_answer_timeout,dst_connect_msg_timeout,src_faststart,src_response_faststart,src_tunneling,src_start_h245_after,dst_ani_type_of_number,dst_ani_numbering_plan,dst_dnis_type_of_number,dst_dnis_numbering_plan,dst_ani_presentation,dst_ani_screening,dst_faststart,dst_tunneling,dst_start_h245_after,dst_report_orig_dest,dst_sip_privacy_method,sip_gate_query,src_debug,dst_debug,stat_enable,dst_cpc_method,disallow_dynamic_payload_type,cancel_src_number_translations,dst_default_protocol,dst_use_display_name,src_codec_policy,dst_codec_policy,src_start_h245_can_be_forced,dst_start_h245_can_be_forced,dst_add_end_of_pulsing,dst_h323_dtmf_policy,sip_reason_policy,radius_force_originate_telephony,dst_sip_router_timeout) ";
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Termination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 0);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, 1720);
                ps.setInt(12, 80);
                ps.setInt(13, 0);
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 10000);
                ps.setInt(17, 32000);
                ps.setInt(18, 90);
                ps.setInt(19, 1);
                ps.setInt(20, 7);
                ps.setInt(21, 1);
                ps.setInt(22, 2);
                ps.setInt(23, -1);
                ps.setInt(24, -1);
                ps.setInt(25, -1);
                ps.setInt(26, -1);
                ps.setInt(27, -2);
                ps.setInt(28, -2);
                ps.setInt(29, 1);
                ps.setInt(30, 1);
                ps.setInt(31, 2);
                ps.setInt(32, 0);
                ps.setInt(33, 1);
                ps.setInt(34, 0);
                ps.setInt(35, 0);
                ps.setInt(36, 1);
                ps.setInt(37, 1);
                ps.setInt(38, 0);
                ps.setInt(39, 0);
                ps.setInt(40, 0);
                ps.setInt(41, 1);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 0);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not added. Please try again.");
                }
            }

            if (p_dto.getGateway_type() == 2) {
                sql = "insert into mvts_gateway(gateway_name,description,equipment_type,enable,src_enable,dst_enable,reg_type,protocol,src_zone,dst_zone,src_address_list,dst_address,dst_port_h323,src_codec_allow,dst_codec_allow,dst_codecs_sort,src_dnis_prefix_allow,src_rbt_enable,dst_h323_first_answer_timeout,dst_sip_first_answer_timeout,dst_connect_msg_timeout,src_faststart,src_response_faststart,src_tunneling,src_start_h245_after,dst_tunneling,dst_faststart,dst_start_h245_after,dst_ani_type_of_number,dst_ani_numbering_plan,dst_dnis_type_of_number,dst_dnis_numbering_plan,dst_ani_presentation,dst_ani_screening,sip_gate_query,src_debug,dst_debug,radius_auth_call_enable,radius_acct_enable,stat_enable,disallow_dynamic_payload_type,src_drop_call_on_alerting_timeout,dst_default_protocol,src_codec_policy,dst_codec_policy,src_start_h245_can_be_forced,dst_start_h245_can_be_forced,dst_add_end_of_pulsing,src_h323_dtmf_policy,dst_h323_dtmf_policy,sip_reason_policy,radius_force_originate_telephony,always_use_ts_conf_id,dst_sip_router_timeout) ";
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Both");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, "voip");
                ps.setString(11, p_dto.getGateway_ip());
                ps.setString(12, p_dto.getGateway_ip());
                ps.setInt(13, 1720);
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 1);
                ps.setInt(17, 90);
                ps.setString(17, prefixes);
                ps.setInt(18, 0);
                ps.setInt(19, 10000);
                ps.setInt(20, 32000);
                ps.setInt(21, 90);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, -1);
                ps.setInt(30, -1);
                ps.setInt(31, -1);
                ps.setInt(32, -1);
                ps.setInt(33, -2);
                ps.setInt(34, -2);
                ps.setInt(35, 0);
                ps.setInt(36, 1);
                ps.setInt(37, 1);
                ps.setInt(38, 0);
                ps.setInt(39, 0);
                ps.setInt(40, 1);
                ps.setInt(41, 0);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 1);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 0);
                ps.setInt(52, 0);
                ps.setInt(53, 0);
                ps.setInt(54, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not added. Please try again.");
                }
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error in MVTSPRO.");
            logger.fatal("Error while adding gateway: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }

            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError editGatewayInformation(GatewayDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select gateway_ip from gateway where gateway_ip = ? and id!=" + p_dto.getId() + " and gateway_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getGateway_ip());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User IP.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update gateway set gateway_ip=?,gateway_name=?,gateway_type=?,gateway_status=?,clientId=?  where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getGateway_ip());
            ps.setString(2, p_dto.getGateway_name());
            ps.setInt(3, p_dto.getGateway_type());
            ps.setInt(4, p_dto.getGateway_status());
            ps.setInt(5, p_dto.getClientId());

            ps.executeUpdate();
            GatewayLoader.getInstance().forceReload();
        }catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing gateway: ", ex);
        }
        try{     
            String prefixes = "";
            String sql = "";
            if (p_dto.getClientId() != 0) {
                com.myapp.struts.clients.ClientDTO clientDTO = com.myapp.struts.clients.ClientLoader.getInstance().getClientDTOByID(p_dto.getClientId());
                ArrayList<com.myapp.struts.rates.RateDTO> rates = com.myapp.struts.rates.RateLoader.getInstance().getRateDTOList();
                for (com.myapp.struts.rates.RateDTO rateDTO : rates) {
                    logger.debug("RAteeee Plan--->" + rateDTO.getRateplan_id());
                    logger.debug("Client Plan--->" + clientDTO.getRateplan_id());
                    if (rateDTO.getRateplan_id() == clientDTO.getRateplan_id()) {
                        prefixes += rateDTO.getRate_destination_code() + "[0-9]*;";
                    }
                }
            }else{
                prefixes = "";
            }
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();           
            Statement stmt = dbConn.connection.createStatement();
            sql = "select gateway_id from mvts_gateway where "
                    + "gateway_name='" + p_dto.getGateway_name() + "'";          
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {
                p_dto.setMvts_id(resultSet.getInt("gateway_id"));
            }
            logger.debug("IDDDDDDDDDDD---->" + p_dto.getMvts_id());
           
            sql="";
            if (p_dto.getGateway_type() == 0) {
                sql = "update mvts_gateway set gateway_name=?,description=?,equipment_type=?,enable=?,src_enable=?,dst_enable=?,reg_type=?,protocol=?,src_zone=?,src_address_list=?,dst_port_h323=?,dst_port_sip=?,src_codec_allow=?,dst_proxy_policy=?,dst_codecs_sort=?,src_dnis_prefix_allow=?,src_rbt_enable=?,dst_h323_first_answer_timeout=?,dst_sip_first_answer_timeout=?,dst_connect_msg_timeout=?,src_can_update_media_channel=?,src_faststart=?,src_response_faststart=?,src_tunneling=?,src_start_h245_after=?,dst_faststart=?,dst_tunneling=?,dst_start_h245_after=?,sip_gate_query=?,src_debug=?,dst_debug=?,radius_auth_call_enable=?,radius_acct_enable=?,stat_enable=?,disallow_dynamic_payload_type=?,src_drop_call_on_alerting_timeout=?,dst_default_protocol=?,src_codec_policy=?,dst_codec_policy=?,src_start_h245_can_be_forced=?,dst_start_h245_can_be_forced=?,dst_add_end_of_pulsing=?,src_h323_dtmf_policy=?,sip_reason_policy=?,radius_force_originate_telephony=?,always_use_ts_conf_id=?,dst_sip_router_timeout=? where ";
                sql += "gateway_id=" + p_dto.getMvts_id();
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Origination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 0);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, 1720);
                ps.setInt(12, 5060);
                ps.setInt(13, 1);
                ps.setInt(14, 5);
                ps.setInt(15, 1);
                ps.setString(16, prefixes);
                ps.setInt(17, 0);
                ps.setInt(18, 10000);
                ps.setInt(19, 32000);
                ps.setInt(20, 90);
                ps.setInt(21, 0);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, 0);
                ps.setInt(30, 1);
                ps.setInt(31, 0);
                ps.setInt(32, 0);
                ps.setInt(33, 0);
                ps.setInt(34, 1);
                ps.setInt(35, 0);
                ps.setInt(36, 0);
                ps.setInt(37, 1);
                ps.setInt(38, 1);
                ps.setInt(39, 1);
                ps.setInt(40, 1);
                ps.setInt(41, 1);
                ps.setInt(42, 1);
                ps.setInt(43, 0);
                ps.setInt(44, 0);
                ps.setInt(45, 0);
                ps.setInt(46, 0);
                ps.setInt(47, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not edited. Please try again.");
                }
            }

            if (p_dto.getGateway_type() == 1) {
                sql = "update mvts_gateway set gateway_name=?,description=?,equipment_type=?,enable=?,src_enable=?,dst_enable=?,reg_type=?,protocol=?,dst_zone=?,dst_address=?,dst_port_h323=?,dst_port_sip=?,dst_proxy_policy=?,dst_codec_allow=?,dst_codecs_sort=?,dst_h323_first_answer_timeout=?,dst_sip_first_answer_timeout=?,dst_connect_msg_timeout=?,src_faststart=?,src_response_faststart=?,src_tunneling=?,src_start_h245_after=?,dst_ani_type_of_number=?,dst_ani_numbering_plan=?,dst_dnis_type_of_number=?,dst_dnis_numbering_plan=?,dst_ani_presentation=?,dst_ani_screening=?,dst_faststart=?,dst_tunneling=?,dst_start_h245_after=?,dst_report_orig_dest=?,dst_sip_privacy_method=?,sip_gate_query=?,src_debug=?,dst_debug=?,stat_enable=?,dst_cpc_method=?,disallow_dynamic_payload_type=?,cancel_src_number_translations=?,dst_default_protocol=?,dst_use_display_name=?,src_codec_policy=?,dst_codec_policy=?,src_start_h245_can_be_forced=?,dst_start_h245_can_be_forced=?,dst_add_end_of_pulsing=?,dst_h323_dtmf_policy=?,sip_reason_policy=?,radius_force_originate_telephony=?,dst_sip_router_timeout=? where ";
                sql += "gateway_id=" + p_dto.getMvts_id();
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Termination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 0);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, 1720);
                ps.setInt(12, 80);
                ps.setInt(13, 0);
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 10000);
                ps.setInt(17, 32000);
                ps.setInt(18, 90);
                ps.setInt(19, 1);
                ps.setInt(20, 7);
                ps.setInt(21, 1);
                ps.setInt(22, 2);
                ps.setInt(23, -1);
                ps.setInt(24, -1);
                ps.setInt(25, -1);
                ps.setInt(26, -1);
                ps.setInt(27, -2);
                ps.setInt(28, -2);
                ps.setInt(29, 1);
                ps.setInt(30, 1);
                ps.setInt(31, 2);
                ps.setInt(32, 0);
                ps.setInt(33, 1);
                ps.setInt(34, 0);
                ps.setInt(35, 0);
                ps.setInt(36, 1);
                ps.setInt(37, 1);
                ps.setInt(38, 0);
                ps.setInt(39, 0);
                ps.setInt(40, 0);
                ps.setInt(41, 1);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 0);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not edited. Please try again.");
                }
            }

            if (p_dto.getGateway_type() == 2) {
                sql = "update mvts_gateway set gateway_name=?,description=?,equipment_type=?,enable=?,src_enable=?,dst_enable=?,reg_type=?,protocol=?,src_zone=?,dst_zone=?,src_address_list=?,dst_address=?,dst_port_h323=?,src_codec_allow=?,dst_codec_allow=?,dst_codecs_sort=?,src_dnis_prefix_allow=?,src_rbt_enable=?,dst_h323_first_answer_timeout=?,dst_sip_first_answer_timeout=?,dst_connect_msg_timeout=?,src_faststart=?,src_response_faststart=?,src_tunneling=?,src_start_h245_after=?,dst_tunneling=?,dst_faststart=?,dst_start_h245_after=?,dst_ani_type_of_number=?,dst_ani_numbering_plan=?,dst_dnis_type_of_number=?,dst_dnis_numbering_plan=?,dst_ani_presentation=?,dst_ani_screening=?,sip_gate_query=?,src_debug=?,dst_debug=?,radius_auth_call_enable=?,radius_acct_enable=?,stat_enable=?,disallow_dynamic_payload_type=?,src_drop_call_on_alerting_timeout=?,dst_default_protocol=?,src_codec_policy=?,dst_codec_policy=?,src_start_h245_can_be_forced=?,dst_start_h245_can_be_forced=?,dst_add_end_of_pulsing=?,src_h323_dtmf_policy=?,dst_h323_dtmf_policy=?,sip_reason_policy=?,radius_force_originate_telephony=?,always_use_ts_conf_id=?,dst_sip_router_timeout=? where ";
                sql += "gateway_id=" + p_dto.getMvts_id();
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Both");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, -1);
                ps.setString(9, "voip");
                ps.setString(10, "voip");
                ps.setString(11, p_dto.getGateway_ip());
                ps.setString(12, p_dto.getGateway_ip());
                ps.setInt(13, 1720);
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 1);
                ps.setInt(17, 90);
                ps.setString(17, prefixes);
                ps.setInt(18, 0);
                ps.setInt(19, 10000);
                ps.setInt(20, 32000);
                ps.setInt(21, 90);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, -1);
                ps.setInt(30, -1);
                ps.setInt(31, -1);
                ps.setInt(32, -1);
                ps.setInt(33, -2);
                ps.setInt(34, -2);
                ps.setInt(35, 0);
                ps.setInt(36, 1);
                ps.setInt(37, 1);
                ps.setInt(38, 0);
                ps.setInt(39, 0);
                ps.setInt(40, 1);
                ps.setInt(41, 0);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 1);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 0);
                ps.setInt(52, 0);
                ps.setInt(53, 0);
                ps.setInt(54, 32000);
                if (ps.executeUpdate() < 1) {
                    error.setErrorMessage("MVTSPRO did not edited. Please try again.");
                }
            }                        
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error in MVTSPRO.");
            logger.fatal("Error while editing gateway: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteGateway(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update gateway set gateway_delete = 1"
                    + " where id =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "select gateway_name from gateway"
                    + " where id =" + cid;
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
            }

            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "delete from mvts_gateway where ";
            sql += "gateway_name='" + dto.getGateway_name() + "'";
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not delete. Please try again.");
            }



        } catch (Exception ex) {
            logger.fatal("Error while editing gateway: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError multipleGatewayStatusUpdate(long gatewayIds[], int gateway_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        remotedbconnector.DBConnection dbConn = null;
        int i = 0;
        int sts = 0;


        String selectedIdsString = Utils.implodeArray(gatewayIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update gateway set gateway_status = " + gateway_status
                    + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }
            
            sql = "select gateway_name from gateway"
                    + " where id in(" + selectedIdsString + ")";
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'"+dto.getGateway_name()+"'";
                logger.debug("multipleNameList:" + multipleNameList[i]);
                i++;
            }

            
            if(gateway_status==0){
                sts = 1;
            }else{
                sts = 0;
            }
            
            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_gateway set enable = " + sts
                    + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not block. Please try again.");
            }
            

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleGatewayDelete(long gatewayIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        remotedbconnector.DBConnection dbConn = null;
        int i = 0;

        String selectedIdsString = Utils.implodeArray(gatewayIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update gateway set gateway_delete = 1"
                    + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }
            sql = "select gateway_name from gateway"
                    + " where id in(" + selectedIdsString + ")";
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'"+dto.getGateway_name()+"'";
                logger.debug("multipleNameList:" + multipleNameList[i]);
                i++;
            }

            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            sql = "delete from mvts_gateway"
                    + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not delete. Please try again.");
            }
        } catch (Exception ex) {
            logger.fatal("Error while deleting gateway ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null && dbConn.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);

                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
