package com.myapp.struts.destcode;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

/**
 *
 * @author Administrator
 */
public class ListDestCodeAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            DestCodeTaskSchedular scheduler = new DestCodeTaskSchedular();
            DestCodeForm destCodeForm = (DestCodeForm) form;

            if (list_all == 0) {
                if (destCodeForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, destCodeForm.getRecordPerPage());
                }
                DestCodeDTO ddto = new DestCodeDTO();
                if (destCodeForm.getDestcode() != null && destCodeForm.getDestcode().trim().length() > 0) {
                    ddto.searchWithDestCode = true;
                    ddto.setDestcode(destCodeForm.getDestcode().toLowerCase());
                }
                if (destCodeForm.getDestcode_name() != null && destCodeForm.getDestcode_name().trim().length() > 0) {
                    ddto.searchWithDestName = true;
                    ddto.setDestcode_name(destCodeForm.getDestcode_name().toLowerCase());
                }
                destCodeForm.setDestcodeList(scheduler.getDestCodeDTOsWithSearchParam(ddto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    destCodeForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                destCodeForm.setDestcodeList(scheduler.getDestCodeDTOs(login_dto));
            }

            if (destCodeForm.getDestcodeList() != null && destCodeForm.getDestcodeList().size() <= (destCodeForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
