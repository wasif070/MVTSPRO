/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.mvtscdr;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class ListClientMvtsCdrAction extends Action {

    static Logger logger = Logger.getLogger(ListMvtsCdrAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null) {
            request.getSession(true).setAttribute(Constants.MESSAGE,null);
            int pageNo = 1;
            String links = "";
            int perPageRecord = Constants.PER_PAGE_RECORD;
            if (request.getParameter("recordPerPage") != null) {
                perPageRecord = Integer.parseInt(request.getParameter("recordPerPage"));
            }
            if (request.getParameter("pageNo") != null) {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            int pageStart = (pageNo - 1) * perPageRecord;

            MvtsCdrTaskSchedular scheduler = new MvtsCdrTaskSchedular();
            MvtsCdrForm mvtsCdrForm = (MvtsCdrForm) form;
            if (request.getParameter("list_all") != null) {
                mvtsCdrForm.setDoSearch(null);
                request.getSession(true).setAttribute("SearchCdrDTO", null);
            }
            if (mvtsCdrForm.getDoSearch() == null) {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    mvtsCdrForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                mvtsCdrForm.setMvtsCdrList(scheduler.getMvtsCdrDTOs(login_dto, pageStart, perPageRecord));
            } else {
                if (mvtsCdrForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, mvtsCdrForm.getRecordPerPage());
                }

                MvtsCdrDTO sdto = new MvtsCdrDTO();

                if (mvtsCdrForm.getOrigination_ip() != null && mvtsCdrForm.getOrigination_ip().length() > 0) {
                    sdto.setOrigination_ip(mvtsCdrForm.getOrigination_ip());
                }

                if (mvtsCdrForm.getTermination_ip() != null && mvtsCdrForm.getTermination_ip().length() > 0) {
                    sdto.setTermination_ip(mvtsCdrForm.getTermination_ip());
                }


                if (mvtsCdrForm.getOriginDestination() != null && mvtsCdrForm.getOriginDestination().length() > 0) {
                    sdto.setOriginDest(mvtsCdrForm.getOriginDestination());
                }

                if (mvtsCdrForm.getTermDestination() != null && mvtsCdrForm.getTermDestination().length() > 0) {
                    sdto.setTermDest(mvtsCdrForm.getOriginDestination());
                }

                if (mvtsCdrForm.getCallType() > 0) {
                    sdto.setCallType(mvtsCdrForm.getCallType());
                }

                String fromDate = mvtsCdrForm.getFromYear() + "-" + formatter.format(mvtsCdrForm.getFromMonth()) + "-" + formatter.format(mvtsCdrForm.getFromDay()) + " " + formatter.format(mvtsCdrForm.getFromHour()) + ":" + formatter.format(mvtsCdrForm.getFromMin()) + ":00";
                sdto.setFromDate(fromDate);
                links += "&fdate=" + fromDate;

                String toDate = mvtsCdrForm.getToYear() + "-" + formatter.format(mvtsCdrForm.getToMonth()) + "-" + formatter.format(mvtsCdrForm.getToDay()) + " " + formatter.format(mvtsCdrForm.getToHour()) + ":" + formatter.format(mvtsCdrForm.getToMin()) + ":00";
                sdto.setToDate(toDate);
                links += "&tdate=" + toDate;

                mvtsCdrForm.setMvtsCdrList(scheduler.getMvtsCdrDTOsWithSearchParam(sdto, login_dto, pageStart, perPageRecord));
                request.getSession(true).setAttribute("SearchCdrDTO", sdto);
            }
            request.getSession(true).setAttribute(mapping.getAttribute(), mvtsCdrForm);
            if (mvtsCdrForm.getMvtsCdrList() != null && mvtsCdrForm.getMvtsCdrList().size() > 0) {
                request.getSession(true).setAttribute("MvtsCdrDTO", mvtsCdrForm.getMvtsCdrList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("MvtsCdrDTO", null);
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}