/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

/**
 *
 * @author Administrator
 */
public class DestCodeDTO {

    public boolean searchWithDestCode = false;
    public boolean searchWithDestName = false;
    private boolean superUser;
    private int id;
    private String destcode;
    private String destcode_name;

    public DestCodeDTO() {
    }

    public String getDestcode() {
        return destcode;
    }

    public void setDestcode(String destcode) {
        this.destcode = destcode;
    }

    public String getDestcode_name() {
        return destcode_name;
    }

    public void setDestcode_name(String destcode_name) {
        this.destcode_name = destcode_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSearchWithDestCode() {
        return searchWithDestCode;
    }

    public void setSearchWithDestCode(boolean searchWithDestCode) {
        this.searchWithDestCode = searchWithDestCode;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }
}
