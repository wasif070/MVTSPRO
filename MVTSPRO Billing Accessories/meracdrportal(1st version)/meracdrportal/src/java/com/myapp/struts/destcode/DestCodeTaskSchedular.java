package com.myapp.struts.destcode;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class DestCodeTaskSchedular {

    public DestCodeTaskSchedular() {
    }

    public MyAppError addDestCodeInformation(DestCodeDTO p_dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        return destDAO.addDestCodeInformation(p_dto);
    }

    public MyAppError uploadFlieDestCode(ArrayList<DestCodeDTO> p_dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        return destDAO.uploadFlieDestCode(p_dto);
    }

    public MyAppError editDestCodeInformation(DestCodeDTO p_dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        return destDAO.editDestCodeInformation(p_dto);
    }

    public MyAppError deleteDestCode(int cid) {
        DestCodeDAO dao = new DestCodeDAO();
        return dao.deleteDestCode(cid);
    }

    public DestCodeDTO getDestCodeDTO(int id) {
        return DestCodeLoader.getInstance().getDestCodeDTOByID(id);
    }

    public ArrayList<DestCodeDTO> getDestCodeDTOsSorted(LoginDTO l_dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        ArrayList<DestCodeDTO> list = DestCodeLoader.getInstance().getDestCodeDTOList();
        if (list != null) {
            return destDAO.getDestCodeDTOsSorted((ArrayList<DestCodeDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<DestCodeDTO> getDestCodeDTOs(LoginDTO l_dto) {
        return DestCodeLoader.getInstance().getDestCodeDTOList();
    }

    public ArrayList<DestCodeDTO> getDestCodeDTOsWithSearchParam(DestCodeDTO udto, LoginDTO l_dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        ArrayList<DestCodeDTO> list = DestCodeLoader.getInstance().getDestCodeDTOList();
        if (list != null) {
            return destDAO.getDestCodeDTOsWithSearchParam((ArrayList<DestCodeDTO>) list.clone(), udto);
        }
        return null;
    }

    public StringBuffer getCSVString(DestCodeDTO dto) {
        DestCodeDAO destDAO = new DestCodeDAO();
        StringBuffer csvString = destDAO.getCSVStrings(dto);
        return csvString;
    }

    public MyAppError deleteMultiple(long destcodeIds[]) {
        DestCodeDAO dao = new DestCodeDAO();
        return dao.multipleDelete(destcodeIds);
    }
}
