package com.myapp.struts.mvtscdr;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class MvtsCdrSummaryAction extends Action {

    static Logger logger = Logger.getLogger(ListMvtsCdrAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && login_dto.getSuperUser()) {
            int pageNo = 1;
            String links = "";

            MvtsCdrTaskSchedular scheduler = new MvtsCdrTaskSchedular();
            MvtsCdrForm mvtsCdrForm = (MvtsCdrForm) form;
            if (mvtsCdrForm.getDoSearch() != null) {
                MvtsCdrDTO sdto = new MvtsCdrDTO();

                if (mvtsCdrForm.getOriginId() > 0) {
                    sdto.setOriginId(mvtsCdrForm.getOriginId());
                    links += "&oid=" + sdto.getOriginId();
                }

                if (mvtsCdrForm.getTermId() > 0) {
                    sdto.setTermId(mvtsCdrForm.getTermId());
                    links += "&tid=" + sdto.getTermId();
                }
                if (mvtsCdrForm.getOrigination_ip() != null && mvtsCdrForm.getOrigination_ip().length() > 0) {
                    sdto.setOrigination_ip(mvtsCdrForm.getOrigination_ip());
                }

                if (mvtsCdrForm.getOriginDestination() != null && mvtsCdrForm.getOriginDestination().length() > 0) {
                    sdto.setOriginDest(mvtsCdrForm.getOriginDestination());
                }

                if (mvtsCdrForm.getTermination_ip() != null && mvtsCdrForm.getTermination_ip().length() > 0) {
                    sdto.setTermination_ip(mvtsCdrForm.getTermination_ip());
                }

                if (mvtsCdrForm.getSummaryType() > -1) {
                    sdto.setSummaryType(mvtsCdrForm.getSummaryType());
                }

                if (mvtsCdrForm.getSummaryBy() != null && mvtsCdrForm.getSummaryBy().length > 0) {
                    sdto.setSummaryBy(mvtsCdrForm.getSummaryBy());
                    String sby = "";
                    if (sdto.getSummaryBy() != null) {
                        for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                            sby += sdto.getSummaryBy()[j] + ",";
                        }
                        links += "&sby=" + sby;
                    }
                }

                String fromDate = mvtsCdrForm.getFromYear() + "-" + formatter.format(mvtsCdrForm.getFromMonth()) + "-" + formatter.format(mvtsCdrForm.getFromDay()) + " " + formatter.format(mvtsCdrForm.getFromHour()) + ":" + formatter.format(mvtsCdrForm.getFromMin()) + ":00";
                sdto.setFromDate(fromDate);
                links += "&fdate=" + fromDate;

                String toDate = mvtsCdrForm.getToYear() + "-" + formatter.format(mvtsCdrForm.getToMonth()) + "-" + formatter.format(mvtsCdrForm.getToDay()) + " " + formatter.format(mvtsCdrForm.getToHour()) + ":" + formatter.format(mvtsCdrForm.getToMin()) + ":00";
                sdto.setToDate(toDate);
                links += "&tdate=" + toDate;

                request.getSession(true).setAttribute(Constants.MESSAGE, null);
                mvtsCdrForm.setMvtsQualityList(scheduler.getSummaryMvtsCdrDTOs(sdto, login_dto));
                request.getSession(true).setAttribute("SearchCdrDTO", sdto);
            } else {
                request.getSession(true).setAttribute(Constants.MESSAGE, "Please Select a Client First");
            }
            request.getSession(true).setAttribute(mapping.getAttribute(), mvtsCdrForm);
            if (mvtsCdrForm.getMvtsQualityList() != null && mvtsCdrForm.getMvtsQualityList().size() > 0) {
                request.getSession(true).setAttribute("MvtsCdrDTO", mvtsCdrForm.getMvtsQualityList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("MvtsCdrDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}