/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.mvtscdr;

/**
 *
 * @author Administrator
 */
public class MvtsCdrDTO {

    public boolean searchOriginClientId = false;
    public boolean searchWithOrigIP = false;
    public boolean searchWithOrigDest = false;
    public boolean searchTermClientId = false;
    public boolean searchWithTermIP = false;
    private long cdr_id;
    private String cdr_date;
    private String in_ani;
    private String out_ani;
    private String in_dnis;
    private String out_dnis;
    private String remote_src_sig_address;
    private String remote_dst_sig_address;
    private int elapsed_time;
    private String setup_time;
    private String connect_time;
    private String disconnect_time;
    private String pdd;
    private long originId;
    private String originClient_id;
    private String originClient_prefix;
    private long termId;
    private String termClient_id;
    private String termClient_prefix;
    private String originDest;
    private String termDest;
    private String origination_ip;
    private String termination_ip;
    private String fromDate;
    private String toDate;
    private int[] summaryBy;
    private int summaryType;
    
    private int callType;

    public MvtsCdrDTO() {
        callType=1;
        summaryType=0;
    }

    public String getCdr_date() {
        return cdr_date;
    }

    public void setCdr_date(String cdr_date) {
        this.cdr_date = cdr_date;
    }

    public long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getConnect_time() {
        return connect_time;
    }

    public void setConnect_time(String connect_time) {
        this.connect_time = connect_time;
    }

    public String getDisconnect_time() {
        return disconnect_time;
    }

    public void setDisconnect_time(String disconnect_time) {
        this.disconnect_time = disconnect_time;
    }

    public int getElapsed_time() {
        return elapsed_time;
    }

    public void setElapsed_time(int elapsed_time) {
        this.elapsed_time = elapsed_time;
    }

    public String getIn_dnis() {
        return in_dnis;
    }

    public void setIn_dnis(String in_dnis) {
        this.in_dnis = in_dnis;
    }

    public String getOut_dnis() {
        return out_dnis;
    }

    public void setOut_dnis(String out_dnis) {
        this.out_dnis = out_dnis;
    }

    public String getRemote_dst_sig_address() {
        return remote_dst_sig_address;
    }

    public void setRemote_dst_sig_address(String remote_dst_sig_address) {
        this.remote_dst_sig_address = remote_dst_sig_address;
    }

    public String getRemote_src_sig_address() {
        return remote_src_sig_address;
    }

    public void setRemote_src_sig_address(String remote_src_sig_address) {
        this.remote_src_sig_address = remote_src_sig_address;
    }

    public String getSetup_time() {
        return setup_time;
    }

    public void setSetup_time(String setup_time) {
        this.setup_time = setup_time;
    }

    public String getPdd() {
        return pdd;
    }

    public void setPdd(String pdd) {
        this.pdd = pdd;
    }

    public String getOriginClient_id() {
        return originClient_id;
    }

    public void setOriginClient_id(String originClient_id) {
        this.originClient_id = originClient_id;
    }

    public String getOriginClient_prefix() {
        return originClient_prefix;
    }

    public void setOriginClient_prefix(String originClient_prefix) {
        this.originClient_prefix = originClient_prefix;
    }

    public String getTermClient_id() {
        return termClient_id;
    }

    public void setTermClient_id(String termClient_id) {
        this.termClient_id = termClient_id;
    }

    public String getTermClient_prefix() {
        return termClient_prefix;
    }

    public void setTermClient_prefix(String termClient_prefix) {
        this.termClient_prefix = termClient_prefix;
    }

    public String getOriginDest() {
        return originDest;
    }

    public void setOriginDest(String originDest) {
        this.originDest = originDest;
    }

    public String getTermDest() {
        return termDest;
    }

    public void setTermDest(String termDest) {
        this.termDest = termDest;
    }

    public String getOrigination_ip() {
        return origination_ip;
    }

    public void setOrigination_ip(String origination_ip) {
        this.origination_ip = origination_ip;
    }

    public String getTermination_ip() {
        return termination_ip;
    }

    public void setTermination_ip(String termination_ip) {
        this.termination_ip = termination_ip;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public long getOriginId() {
        return originId;
    }

    public void setOriginId(long originId) {
        this.originId = originId;
    }

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public int[] getSummaryBy() {
        return summaryBy;
    }

    public void setSummaryBy(int[] summaryBy) {
        this.summaryBy = summaryBy;
    }

   

    public int getSummaryType() {
        return summaryType;
    }

    public void setSummaryType(int summaryType) {
        this.summaryType = summaryType;
    }

    public String getIn_ani() {
        return in_ani;
    }

    public void setIn_ani(String in_ani) {
        this.in_ani = in_ani;
    }

    public String getOut_ani() {
        return out_ani;
    }

    public void setOut_ani(String out_ani) {
        this.out_ani = out_ani;
    }
}