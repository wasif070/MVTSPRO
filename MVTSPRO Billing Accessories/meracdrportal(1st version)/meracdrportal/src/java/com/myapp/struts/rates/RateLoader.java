/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class RateLoader {

    static Logger logger = Logger.getLogger(RateLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<RateDTO> rateList = null;
    private HashMap<Long, RateDTO> rateDTOByID = null;
    private HashMap<Long, ArrayList<RateDTO>> rateDTOByRateID = null;
    private HashMap<Long, ArrayList<RateDTO>> rateDTORatePlanID = null;
    static RateLoader destcodeLoader = null;

    public RateLoader() {
        forceReload();
    }

    public static RateLoader getInstance() {
        if (destcodeLoader == null) {
            createGatewayLoader();
        }
        return destcodeLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (destcodeLoader == null) {
            destcodeLoader = new RateLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        Statement st1 = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            st1 = dbConnection.connection.createStatement();
            rateList = new ArrayList<RateDTO>();
            rateDTOByID = new HashMap<Long, RateDTO>();
            rateDTOByRateID = new HashMap<Long, ArrayList<RateDTO>>();
            rateDTORatePlanID = new HashMap<Long, ArrayList<RateDTO>>();
            String sql = "select * from mvts_rates where rate_delete=0";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                RateDTO dto = new RateDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));

                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
                ArrayList<RateDTO> rateDtoListByRateId = rateDTOByRateID.get(dto.getRate_id());
                if (rateDtoListByRateId == null) {
                    rateDtoListByRateId = new ArrayList<RateDTO>();
                }
                rateDtoListByRateId.add(dto);

                ArrayList<RateDTO> rateplanRateDtoList = rateDTORatePlanID.get(dto.getRateplan_id());
                if (rateplanRateDtoList == null) {
                    rateplanRateDtoList = new ArrayList<RateDTO>();
                }

                rateplanRateDtoList.add(dto);
                rateDTORatePlanID.put(dto.getRateplan_id(), rateplanRateDtoList);
                rateDTOByRateID.put(dto.getRate_id(), rateDtoListByRateId);
                rateDTOByID.put(dto.getId(), dto);
                rateList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in RateLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (st1 != null) {
                    st1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<RateDTO> getRateDTOList() {
        checkForReload();
        return rateList;
    }

    public synchronized RateDTO getRateDTOByID(long id) {
        checkForReload();
        return rateDTOByID.get(id);
    }

    public synchronized ArrayList<RateDTO> getRateDTOListByPlan(long plan_id) {
        checkForReload();
        RateDAO dao = new RateDAO();
        ArrayList<RateDTO> rateListByPlan = dao.getRateDTOsSorted(rateDTORatePlanID.get(plan_id));
        ArrayList<RateDTO> mlist = new ArrayList<RateDTO>();
        long rate_id = 0;
        if (rateListByPlan != null) {
            for (int inc = 0; inc < rateListByPlan.size(); inc++) {
                RateDTO rdto = rateListByPlan.get(inc);
                if (rate_id == rdto.getRate_id()) {
                    continue;
                }
                rate_id = rdto.getRate_id();
                mlist.add(rdto);
            }
        }
        return mlist;
    }

    public synchronized ArrayList<RateDTO> getRateDTOAllByPlan(long plan_id) {
        return rateDTORatePlanID.get(plan_id);
    }

    public synchronized ArrayList<RateDTO> getRateDTOByRateIdList(long rate_id) {
        checkForReload();
        return rateDTOByRateID.get(rate_id);
    }

    public synchronized RateDTO getRateDTOByRateId(long rate_id) {
        checkForReload();
        ArrayList<RateDTO> rlist = rateDTOByRateID.get(rate_id);
        return rlist.get(0);
    }
}
