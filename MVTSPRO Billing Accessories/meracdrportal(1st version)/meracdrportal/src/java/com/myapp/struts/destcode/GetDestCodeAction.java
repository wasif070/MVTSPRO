/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class GetDestCodeAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        int id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            DestCodeForm formBean = (DestCodeForm) form;
            DestCodeDTO dto = new DestCodeDTO();
            DestCodeTaskSchedular scheduler = new DestCodeTaskSchedular();
            dto = scheduler.getDestCodeDTO(id);
            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setDestcode(dto.getDestcode());
                formBean.setDestcode_name(dto.getDestcode_name());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}