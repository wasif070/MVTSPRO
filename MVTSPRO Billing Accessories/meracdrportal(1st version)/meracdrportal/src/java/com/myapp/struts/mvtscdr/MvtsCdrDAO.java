/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.mvtscdr;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.destcode.DestCodeDTO;
import com.myapp.struts.destcode.DestCodeLoader;
import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MvtsCdrDAO {

    static Logger logger = Logger.getLogger(MvtsCdrDAO.class.getName());

    public MvtsCdrDAO() {
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrDTOsWithSearchParam(MvtsCdrDTO sdto, int start, int end) {
        String condition = "elapsed_time >0";
        if (sdto.getCallType() == 2) {
            condition = "elapsed_time is null";
        } else {
            if (sdto.getFromDate() != null) {
                condition += " and connect_time >='" + sdto.getFromDate() + "'";
            } else {
                condition += " and connect_time >=CURDATE()";
            }

            if (sdto.getToDate() != null) {
                condition += " and connect_time <='" + sdto.getToDate() + "'";
            } else {
                condition += " and connect_time <=NOW()";
            }
        }
        if (sdto.getOriginId() > 0 && sdto.getOrigination_ip().equals("0")) {
            ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getOriginId());
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getOrigination_ip() != null && !sdto.getOrigination_ip().equals("0")) {
                condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
            }
        }

        if (sdto.getTermId() > 0 && sdto.getTermination_ip().equals("0")) {
            ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getTermId());
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getTermination_ip() != null && !sdto.getTermination_ip().equals("0")) {
                condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
            }
        }

        if (sdto.getOriginId() > 0 && sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
            ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(sdto.getOriginId());
            condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
        }

        DBConnection dbConnection = null;
        Statement statement = null;
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            mvtsCdrList = new ArrayList<MvtsCdrDTO>();
            String sql = "select cdr_id,in_ani,out_ani,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,disconnect_time from mvts_cdr "
                    + " where  " + condition + " limit " + start + "," + end;

            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {
                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setIn_ani(resultSet.getString("in_ani"));
                dto.setOut_ani(resultSet.getString("out_ani"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                    } else {
                        dto.setOriginClient_id("");
                    }

                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                    } else {
                        dto.setTermClient_id("");
                    }

                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrDTOList(int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "elapsed_time >0 and connect_time >= CURDATE() and connect_time <= NOW()";
        //condition = "elapsed_time >0";
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select cdr_id,in_ani,out_ani,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,disconnect_time from mvts_cdr"
                    + " where " + condition + " limit " + start + "," + end;
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {

                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setIn_ani(resultSet.getString("in_ani"));
                dto.setOut_ani(resultSet.getString("out_ani"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                    } else {
                        dto.setOriginClient_id("");
                    }
                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                    } else {
                        dto.setTermClient_id("");
                    }
                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public int getTotalMvtsCDR(MvtsCdrDTO sdto) {

        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;

        String condition = ""; //elapsed_time >0";
        condition = "elapsed_time >0";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = "elapsed_time is null";
            } else {
                if (sdto.getFromDate() != null) {
                    condition += " and connect_time >='" + sdto.getFromDate() + "'";
                } else {
                    condition += " and connect_time >=CURDATE()";
                }

                if (sdto.getToDate() != null) {
                    condition += " and connect_time <='" + sdto.getToDate() + "'";
                } else {
                    condition += " and connect_time <=NOW()";
                }
            }

            if (sdto.getOriginId() > 0 && sdto.getOrigination_ip().equals("0")) {
                ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getOriginId());
                if (clientGateway != null) {
                    for (int j = 0; j < clientGateway.size(); j++) {
                        GatewayDTO gdto = clientGateway.get(j);
                        if (gdto != null) {
                            if (j == 0) {
                                condition += " and (";
                            } else {
                                condition += " or ";
                            }
                            switch (gdto.getGateway_type()) {
                                case Constants.ORIGINATION:
                                    condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                    break;
                                case Constants.TERMINATION:
                                    condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                    break;
                                case Constants.BOTH:
                                    condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                    break;
                            }
                            if (j == clientGateway.size() - 1) {
                                condition += " ) ";
                            }
                        }
                    }
                }
            } else {
                if (sdto.getOrigination_ip() != null && !sdto.getOrigination_ip().equals("0")) {
                    condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
                }
            }

            if (sdto.getTermId() > 0 && sdto.getTermination_ip().equals("0")) {
                ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getTermId());
                if (clientGateway != null) {
                    for (int j = 0; j < clientGateway.size(); j++) {
                        GatewayDTO gdto = clientGateway.get(j);
                        if (gdto != null) {
                            if (j == 0) {
                                condition += " and (";
                            } else {
                                condition += " or ";
                            }
                            switch (gdto.getGateway_type()) {
                                case Constants.ORIGINATION:
                                    condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                    break;
                                case Constants.TERMINATION:
                                    condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                    break;
                                case Constants.BOTH:
                                    condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                    break;
                            }
                            if (j == clientGateway.size() - 1) {
                                condition += " ) ";
                            }
                        }
                    }
                }
            } else {
                if (sdto.getTermination_ip() != null && !sdto.getTermination_ip().equals("0")) {
                    condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
                }
            }

            if (sdto.getOriginId() > 0 && sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
                ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(sdto.getOriginId());
                condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
            }
        } else {
            condition = "elapsed_time >0 and connect_time >= CURDATE() and connect_time <=NOW()";
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from mvts_cdr where " + condition;
            logger.debug(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }

    public int getClientTotalMvtsCDR(long client_id, MvtsCdrDTO sdto) {
        ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(client_id);
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;
        if (clientGateway == null || clientGateway.size() < 1) {
            return totalRecord;
        }
        String condition = ""; //"elapsed_time >0";
        condition = "elapsed_time >0 and connect_time >= CURDATE() and connect_time <=NOW()";
        if (sdto != null) {
            condition = "elapsed_time >0";
            if (sdto.getCallType() == 2) {
                condition = "elapsed_time is null";
                if (sdto.getFromDate() != null) {
                    condition += " and disconnect_time >='" + sdto.getFromDate() + "'";
                } else {
                    condition += " and disconnect_time >=CURDATE()";
                }
                if (sdto.getToDate() != null) {
                    condition += " and disconnect_time <='" + sdto.getToDate() + "'";
                } else {
                    condition += " and disconnect_time <=NOW()";
                }
            } else {
                if (sdto.getFromDate() != null) {
                    condition += " and connect_time >='" + sdto.getFromDate() + "'";
                } else {
                    condition += " and connect_time >=CURDATE()";
                }
                if (sdto.getToDate() != null) {
                    condition += " and connect_time <='" + sdto.getToDate() + "'";
                } else {
                    condition += " and connect_time <=NOW()";
                }
            }
            if ((sdto.getOrigination_ip() != null && sdto.getOrigination_ip().equals("0")) || sdto.getTermination_ip() != null && sdto.getTermination_ip().equals("0")) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            } else {
                if (sdto.getOrigination_ip() != null) {
                    condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
                }
                if (sdto.getTermination_ip() != null) {
                    condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
                }
            }

            if (sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
                condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
            }
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from mvts_cdr where " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrDTOByClient(long client_id, int start, int end) {

        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        String condition = "";
        condition = "elapsed_time >0 and connect_time >= CURDATE() and connect_time <=NOW()";
        //condition = "elapsed_time >0";
        if (clientGateway != null && clientGateway.size() > 0) {
            for (int j = 0; j < clientGateway.size(); j++) {
                GatewayDTO gdto = clientGateway.get(j);
                if (gdto != null) {
                    if (j == 0) {
                        condition += " and (";
                    } else {
                        condition += " or ";
                    }
                    switch (gdto.getGateway_type()) {
                        case Constants.ORIGINATION:
                            condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.TERMINATION:
                            condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.BOTH:
                            condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                            break;
                    }
                    if (j == clientGateway.size() - 1) {
                        condition += " ) ";
                    }
                }
            }
        } else {
            return null;
        }

        DBConnection dbConnection = null;
        Statement statement = null;
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            mvtsCdrList = new ArrayList<MvtsCdrDTO>();
            String sql = "select cdr_id,cdr_date,in_ani,out_ani,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,disconnect_time from mvts_cdr "
                    + " where  " + condition + " limit " + start + "," + end;
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {
                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setIn_ani(resultSet.getString("in_ani"));
                dto.setOut_ani(resultSet.getString("out_ani"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                    } else {
                        dto.setOriginClient_id("");
                    }

                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                    } else {
                        dto.setTermClient_id("");
                    }
                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return mvtsCdrList;
    }

    public String getClientGateway(long client_id, int type) {
        String output = "";
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        if (clientGateway != null && clientGateway.size() > 0) {
            int size = clientGateway.size();
            for (int i = 0; i < size; i++) {
                GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                if (c_dto.getGateway_type() == type || c_dto.getGateway_type() == 2) {
                    output += "<option value=\"" + c_dto.getGateway_ip() + "\">" + c_dto.getGateway_ip() + "</option>";
                }
            }
        }
        return output;
    }

    public ArrayList<MvtsCdrDTO> getClientMvtsCdrDTOsWithSearchParam(long client_id, MvtsCdrDTO sdto, int start, int end) {
        ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(client_id);
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        if (clientGateway != null && clientGateway.size() > 0) {
            String condition = "";
            condition = "elapsed_time >0";
            if (sdto.getCallType() == 2) {
                condition = "elapsed_time is null";
                if (sdto.getFromDate() != null) {
                    condition += " and disconnect_time >='" + sdto.getFromDate() + "'";
                } else {
                    condition += " and disconnect_time >=CURDATE()";
                }
                if (sdto.getToDate() != null) {
                    condition += " and disconnect_time <='" + sdto.getToDate() + "'";
                } else {
                    condition += " and disconnect_time <=NOW()";
                }
            } else {
                if (sdto.getFromDate() != null) {
                    condition += " and connect_time >='" + sdto.getFromDate() + "'";
                } else {
                    condition += " and connect_time >=CURDATE()";
                }
                if (sdto.getToDate() != null) {
                    condition += " and connect_time <='" + sdto.getToDate() + "'";
                } else {
                    condition += " and connect_time <=NOW()";
                }
            }
            if ((sdto.getOrigination_ip() != null && sdto.getOrigination_ip().equals("0")) || (sdto.getTermination_ip() != null && sdto.getTermination_ip().equals("0"))) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            } else {
                if (sdto.getOrigination_ip() != null && !sdto.getOrigination_ip().equals("0")) {
                    condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
                }
                if (sdto.getTermination_ip() != null && !sdto.getTermination_ip().equals("0")) {
                    condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
                }

            }

            if (sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
                condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
            }

            if (sdto.getTermDest() != null && sdto.getTermDest().length() > 0) {
                condition += " and out_dnis like '" + clientDto.getOutgoing_prefix() + sdto.getTermDest() + "%'";
            }

            DBConnection dbConnection = null;
            Statement statement = null;

            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                mvtsCdrList = new ArrayList<MvtsCdrDTO>();
                String sql = "select cdr_id,out_ani,in_ani,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,disconnect_time from mvts_cdr "
                        + " where  " + condition + " limit " + start + "," + end;
                logger.debug(sql);
                ResultSet resultSet = statement.executeQuery(sql);
                NumberFormat formatter = new DecimalFormat("#00.00");
                while (resultSet.next()) {
                    MvtsCdrDTO dto = new MvtsCdrDTO();
                    dto.setCdr_id(resultSet.getLong("cdr_id"));
                    dto.setIn_ani(resultSet.getString("in_ani"));
                    dto.setOut_ani(resultSet.getString("out_ani"));
                    dto.setCdr_date(resultSet.getString("cdr_date"));

                    dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                    dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                    dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                    dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                    dto.setSetup_time(resultSet.getString("setup_time"));

                    if (resultSet.getString("connect_time") != null) {
                        dto.setConnect_time(resultSet.getString("connect_time"));
                    } else {
                        dto.setConnect_time(resultSet.getString("disconnect_time"));
                    }

                    ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                    String dialNo = resultSet.getString("in_dnis");
                    if (originCDto != null) {
                        if (originCDto.getClient_id() != null) {
                            dto.setOriginClient_id(originCDto.getClient_id());
                        } else {
                            dto.setOriginClient_id("");
                        }

                        if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                            dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                            dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                        } else {
                            dto.setOriginClient_prefix("");
                        }
                        dto.setIn_dnis(dialNo);
                    } else {
                        dto.setOriginClient_id("");
                        dto.setOriginClient_prefix("");
                        dto.setIn_dnis(dialNo);
                    }
                    if (dto.getIn_dnis() != null) {
                        DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                        if (dcDto.getDestcode() != null) {
                            dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                        } else {
                            dto.setOriginDest("");
                        }

                    } else {
                        dto.setOriginDest("");
                    }

                    ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                    dialNo = resultSet.getString("out_dnis");
                    if (termCDto != null) {
                        if (termCDto.getClient_id() != null) {
                            dto.setTermClient_id(termCDto.getClient_id());
                        } else {
                            dto.setTermClient_id("");
                        }

                        if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                            dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                            dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                        } else {
                            dto.setTermClient_prefix("");
                        }
                        dto.setOut_dnis(dialNo);
                    } else {
                        dto.setTermClient_prefix("");
                        dto.setTermClient_id("");
                        dto.setOut_dnis(dialNo);
                    }
                    if (dto.getOut_dnis() != null) {
                        DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                        if (dcOutDto.getDestcode() != null) {
                            dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                        } else {
                            dto.setTermDest("");
                        }

                    } else {
                        dto.setTermDest("");
                    }
                    mvtsCdrList.add(dto);
                }
                resultSet.close();
            } catch (Exception e) {
                logger.fatal("Exception in MvtsCdrLoader:", e);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<MvtsQualityDTO> getSummaryMvtsCdrDTOList(MvtsCdrDTO sdto) {
        ArrayList<MvtsQualityDTO> qualityMvtsDtoList = new ArrayList<MvtsQualityDTO>();
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();

        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "1";
        if (sdto.getFromDate() != null) {
            condition += " and (connect_time >='" + sdto.getFromDate() + "' or disconnect_time >='" + sdto.getFromDate() + "')";
        } else {
            condition += " and (connect_time >=CURDATE() or disconnect_time>=CURDATE())";
        }

        if (sdto.getToDate() != null) {
            condition += " and (connect_time <='" + sdto.getToDate() + "' or disconnect_time <='" + sdto.getToDate() + "')";
        } else {
            condition += " and (connect_time <=NOW() or disconnect_time <=NOW())";
        }

        if (sdto.getOriginId() > 0 && sdto.getOrigination_ip().equals("0")) {
            ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getOriginId());
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getOrigination_ip() != null && !sdto.getOrigination_ip().equals("0")) {
                condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
            }
        }

        if (sdto.getTermId() > 0 && sdto.getTermination_ip().equals("0")) {
            ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(sdto.getTermId());
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getTermination_ip() != null && !sdto.getTermination_ip().equals("0")) {
                condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
            }
        }
        if (sdto.getOriginId() > 0 && sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
            ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(sdto.getOriginId());
            condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
        }

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            mvtsCdrList = new ArrayList<MvtsCdrDTO>();
            String sql = "select cdr_id,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,connect_time,pdd,disconnect_time from mvts_cdr "
                    + " where  " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {
                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                        dto.setOriginId(originCDto.getId());
                    } else {
                        dto.setOriginClient_id("");
                        dto.setOriginId(0);
                    }

                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                        dto.setTermId(termCDto.getId());
                    } else {
                        dto.setTermClient_id("");
                        dto.setTermId(0);
                    }

                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        if (mvtsCdrList != null) {
            if (sdto.getOriginId() > 0) {
                qualityMvtsDtoList = getMvtsDTOHashMapOriginClient(mvtsCdrList, sdto);
            } else if (sdto.getTermId() > 0) {
                qualityMvtsDtoList = getMvtsDTOHashMapTermClient(mvtsCdrList, sdto);
            }
        }

        return qualityMvtsDtoList;
    }

    public ArrayList<MvtsQualityDTO> getClientSummaryMvtsCdrDTOList(MvtsCdrDTO sdto, long client_id) {
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        if (clientGateway == null || clientGateway.size() < 1) {
            return null;
        }
        ArrayList<MvtsQualityDTO> qualityMvtsDtoList = new ArrayList<MvtsQualityDTO>();
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        ClientDTO clientDto = ClientLoader.getInstance().getClientDTOByID(client_id);
        sdto.setOriginId(clientDto.getId());
        sdto.setOriginClient_id(clientDto.getClient_id());
        sdto.setTermId(clientDto.getId());
        sdto.setTermClient_id(clientDto.getClient_id());

        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "1";
        if (sdto.getFromDate() != null) {
            condition += " and (connect_time >='" + sdto.getFromDate() + "' or disconnect_time >='" + sdto.getFromDate() + "')";
        } else {
            condition += " and (connect_time >=CURDATE() or disconnect_time>=CURDATE())";
        }

        if (sdto.getToDate() != null) {
            condition += " and (connect_time <='" + sdto.getToDate() + "' or disconnect_time <='" + sdto.getToDate() + "')";
        } else {
            condition += " and (connect_time <=NOW() or disconnect_time <=NOW())";
        }

        if (client_id > 0 && sdto.getOrigination_ip() != null && sdto.getOrigination_ip().equals("0")) {
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getOrigination_ip() != null && !sdto.getOrigination_ip().equals("0")) {
                condition += " and remote_src_sig_address like '" + sdto.getOrigination_ip() + "%'";
            }
        }

        if (client_id > 0 && sdto.getTermination_ip() != null && sdto.getTermination_ip().equals("0")) {
            if (clientGateway != null) {
                for (int j = 0; j < clientGateway.size(); j++) {
                    GatewayDTO gdto = clientGateway.get(j);
                    if (gdto != null) {
                        if (j == 0) {
                            condition += " and (";
                        } else {
                            condition += " or ";
                        }
                        switch (gdto.getGateway_type()) {
                            case Constants.ORIGINATION:
                                condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.TERMINATION:
                                condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                                break;
                            case Constants.BOTH:
                                condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                                break;
                        }
                        if (j == clientGateway.size() - 1) {
                            condition += " ) ";
                        }
                    }
                }
            }
        } else {
            if (sdto.getTermination_ip() != null && !sdto.getTermination_ip().equals("0")) {
                condition += " and remote_dst_sig_address like '" + sdto.getTermination_ip() + "%'";
            }
        }
        if (sdto.getOriginId() > 0 && sdto.getOriginDest() != null && sdto.getOriginDest().length() > 0) {
            condition += " and in_dnis like '" + clientDto.getIncoming_prefix() + sdto.getOriginDest() + "%'";
        }

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            mvtsCdrList = new ArrayList<MvtsCdrDTO>();
            String sql = "select cdr_id,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,connect_time,pdd,disconnect_time from mvts_cdr "
                    + " where  " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {
                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                        dto.setOriginId(originCDto.getId());
                    } else {
                        dto.setOriginClient_id("");
                        dto.setOriginId(0);
                    }

                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                        dto.setTermId(termCDto.getId());
                    } else {
                        dto.setTermClient_id("");
                        dto.setTermId(0);
                    }

                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        if (mvtsCdrList != null) {
            switch (clientDto.getClient_type()) {
                case Constants.ORIGINATION:
                case Constants.BOTH:
                    qualityMvtsDtoList = getMvtsDTOHashMapOriginClient(mvtsCdrList, sdto);
                    break;
                case Constants.TERMINATION:
                    qualityMvtsDtoList = getMvtsDTOHashMapTermClient(mvtsCdrList, sdto);
                    break;
            }
        }

        return qualityMvtsDtoList;
    }

    public ArrayList<MvtsQualityDTO> getMvtsDTOHashMapOriginClient(ArrayList<MvtsCdrDTO> list, MvtsCdrDTO sdto) {
        HashMap<Long, ArrayList<MvtsQualityDTO>> qualityDtos = new HashMap<Long, ArrayList<MvtsQualityDTO>>();
        ArrayList<MvtsQualityDTO> returnedDtos = new ArrayList<MvtsQualityDTO>();
        ArrayList<ArrayList<MvtsQualityDTO>> tempDtos = new ArrayList<ArrayList<MvtsQualityDTO>>();

        boolean searchOriginIp = false;
        boolean searchOriginDest = false;
        boolean searchTermIp = false;
        boolean searchTermDest = false;

        if (sdto.getSummaryBy() != null) {
            for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                switch (sdto.getSummaryBy()[j]) {
                    case 0:
                        searchOriginIp = true;
                        break;
                    case 1:
                        searchOriginDest = true;
                    case 2:
                        searchTermIp = true;
                        break;
                    case 3:
                        searchTermDest = true;
                        break;
                }
            }
        }
        long inc_time = 0;
        if (sdto != null && sdto.getSummaryType() > 0) {
            switch (sdto.getSummaryType()) {
                case 1:
                    inc_time = 900000;
                    break;
                case 2:
                    inc_time = 3600000;
                    break;
                case 3:
                    inc_time = 86400000;
                    break;
                case 4:
                    inc_time = 604800000;
                    break;
            }
        }

        boolean isMatch = false;
        if (list != null && list.size() > 0) {
            for (int inc = 0; inc < list.size(); inc++) {
                MvtsCdrDTO mcdr = new MvtsCdrDTO();
                mcdr = list.get(inc);

                MvtsQualityDTO temp = new MvtsQualityDTO();
                temp.setOriginId(mcdr.getOriginId());
                temp.setOriginClient(mcdr.getOriginClient_id());
                temp.setOrigin_dest(mcdr.getOriginDest());
                temp.setOrigination_ip(mcdr.getRemote_src_sig_address());
                temp.setTermId(mcdr.getTermId());
                temp.setTermClient(mcdr.getTermClient_id());
                temp.setTermination_ip(mcdr.getRemote_dst_sig_address());
                temp.setTerm_dest(mcdr.getTermDest());
                temp.setDuration(mcdr.getElapsed_time());
                temp.setTotal_fail(0);
                temp.setTotal_success(0);
                temp.setPeriod_start(Utils.getDateLong(mcdr.getConnect_time()));
                temp.setPeriod_end(Utils.getDateLong(mcdr.getConnect_time()));

                double pdd = Double.parseDouble(mcdr.getPdd());
                temp.setAvg_pdd(pdd);


                long key = temp.getOriginId();
                ArrayList<MvtsQualityDTO> qualityList = qualityDtos.get(key);
                if (qualityList == null) {
                    qualityList = new ArrayList<MvtsQualityDTO>();
                }

                if (qualityList != null && qualityList.size() > 0) {
                    for (int j = 0; j < qualityList.size(); j++) {
                        MvtsQualityDTO d = new MvtsQualityDTO();
                        d = qualityList.get(j);
                        if ((searchOriginIp && !d.getOrigination_ip().equals(temp.getOrigination_ip()))
                                || (searchOriginDest && !d.getOrigin_dest().equals(temp.getOrigin_dest()))
                                || (searchTermIp && !d.getTermination_ip().equals(temp.getTermination_ip()))
                                || (searchTermDest && !d.getTerm_dest().equals(temp.getTerm_dest()))) {
                            isMatch = false;
                            continue;
                        } else {
                            if (sdto.getSummaryType() > 0) {
                                if (temp.getPeriod_start() >= d.getPeriod_start()
                                        && temp.getPeriod_start() <= d.getPeriod_end()) {
                                    isMatch = true;
                                    if (temp.getDuration() > 0) {
                                        temp.setTotal_success(1);
                                        temp.setTotal_fail(0);
                                    } else {
                                        temp.setTotal_success(0);
                                        temp.setTotal_fail(1);
                                    }

                                    int dur = d.getDuration() + temp.getDuration();
                                    d.setDuration(dur);
                                    d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                    int suc = d.getTotal_success() + temp.getTotal_success();
                                    d.setTotal_success(suc);
                                    d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());

                                    if (searchOriginIp) {
                                        d.setOrigination_ip(temp.getOrigination_ip());
                                    } else {
                                        d.setOrigination_ip("");
                                    }
                                    if (searchOriginDest) {
                                        d.setOrigin_dest(temp.getOrigin_dest());
                                    } else {
                                        d.setOrigin_dest("");
                                    }

                                    if (searchTermIp) {
                                        d.setTermination_ip(temp.getTermination_ip());
                                    } else {
                                        d.setTermination_ip("");
                                    }

                                    if (searchTermDest) {
                                        d.setTerm_dest(temp.getTerm_dest());
                                    } else {
                                        d.setTerm_dest("");
                                    }

                                    break;
                                } else {
                                    isMatch = false;
                                    continue;
                                }
                            } else {
                                isMatch = true;
                                if (temp.getDuration() > 0) {
                                    temp.setTotal_success(1);
                                    temp.setTotal_fail(0);
                                } else {
                                    temp.setTotal_success(0);
                                    temp.setTotal_fail(1);
                                }

                                int dur = d.getDuration() + temp.getDuration();
                                d.setDuration(dur);
                                d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                int suc = d.getTotal_success() + temp.getTotal_success();
                                d.setTotal_success(suc);
                                d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                d.setPeriod_end(temp.getPeriod_start());
                                if (searchOriginIp) {
                                    d.setOrigination_ip(temp.getOrigination_ip());
                                } else {
                                    d.setOrigination_ip("");
                                }
                                if (searchOriginDest) {
                                    d.setOrigin_dest(temp.getOrigin_dest());
                                } else {
                                    d.setOrigin_dest("");
                                }

                                if (searchTermIp) {
                                    d.setTermination_ip(temp.getTermination_ip());
                                } else {
                                    d.setTermination_ip("");
                                }

                                if (searchTermDest) {
                                    d.setTerm_dest(temp.getTerm_dest());
                                } else {
                                    d.setTerm_dest("");
                                }
                                break;
                            }
                        }
                    }
                }
                if (!isMatch) {
                    long end = temp.getPeriod_end() + inc_time;
                    temp.setPeriod_end(end);

                    if (temp.getDuration() > 0) {
                        temp.setTotal_success(1);
                        temp.setTotal_fail(0);
                    } else {
                        temp.setTotal_success(0);
                        temp.setTotal_fail(1);
                    }
                    qualityList.add(temp);
                }
                qualityDtos.put(key, qualityList);
            }
        }
        long mapkey = sdto.getOriginId();
        if (mapkey > 0 && qualityDtos.get(mapkey) != null) {
            returnedDtos.addAll(qualityDtos.get(mapkey));
        } else {
            tempDtos.addAll(qualityDtos.values());
            if (tempDtos != null && tempDtos.size() > 0) {
                for (int i = 0; i < tempDtos.size(); i++) {
                    returnedDtos.addAll(tempDtos.get(i));
                }
                logger.debug("Temp Size-->" + tempDtos.size());
            }
        }

        if (returnedDtos != null) {
            return getMvtsDTOsSorted((ArrayList<MvtsQualityDTO>) returnedDtos.clone());
        } else {
            return returnedDtos;
        }
    }

    public ArrayList<MvtsQualityDTO> getMvtsDTOHashMapTermClient(ArrayList<MvtsCdrDTO> list, MvtsCdrDTO sdto) {
        HashMap<Long, ArrayList<MvtsQualityDTO>> qualityDtos = new HashMap<Long, ArrayList<MvtsQualityDTO>>();
        ArrayList<MvtsQualityDTO> returnedDtos = new ArrayList<MvtsQualityDTO>();
        ArrayList<ArrayList<MvtsQualityDTO>> tempDtos = new ArrayList<ArrayList<MvtsQualityDTO>>();

        boolean searchOriginIp = false;
        boolean searchOriginDest = false;
        boolean searchTermIp = false;
        boolean searchTermDest = false;

        if (sdto.getSummaryBy() != null) {
            for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                switch (sdto.getSummaryBy()[j]) {
                    case 0:
                        searchOriginIp = true;
                        break;
                    case 1:
                        searchOriginDest = true;
                    case 2:
                        searchTermIp = true;
                        break;
                    case 3:
                        searchTermDest = true;
                        break;
                }
            }
        }
        long inc_time = 0;
        if (sdto != null && sdto.getSummaryType() > 0) {
            switch (sdto.getSummaryType()) {
                case 1:
                    inc_time = 900000;
                    break;
                case 2:
                    inc_time = 3600000;
                    break;
                case 3:
                    inc_time = 86400000;
                    break;
                case 4:
                    inc_time = 604800000;
                    break;
            }
        }

        boolean isMatch = false;
        if (list != null && list.size() > 0) {
            for (int inc = 0; inc < list.size(); inc++) {
                MvtsCdrDTO mcdr = new MvtsCdrDTO();
                mcdr = list.get(inc);

                MvtsQualityDTO temp = new MvtsQualityDTO();
                temp.setOriginId(mcdr.getOriginId());
                temp.setOriginClient(mcdr.getOriginClient_id());
                temp.setOrigin_dest(mcdr.getOriginDest());
                temp.setOrigination_ip(mcdr.getRemote_src_sig_address());
                temp.setTermId(mcdr.getTermId());
                temp.setTermClient(mcdr.getTermClient_id());
                temp.setTermination_ip(mcdr.getRemote_dst_sig_address());
                temp.setTerm_dest(mcdr.getTermDest());
                temp.setDuration(mcdr.getElapsed_time());
                temp.setTotal_fail(0);
                temp.setTotal_success(0);
                temp.setPeriod_start(Utils.getDateLong(mcdr.getConnect_time()));
                temp.setPeriod_end(Utils.getDateLong(mcdr.getConnect_time()));

                double pdd = Double.parseDouble(mcdr.getPdd());
                temp.setAvg_pdd(pdd);


                long key = temp.getTermId();
                ArrayList<MvtsQualityDTO> qualityList = qualityDtos.get(key);
                if (qualityList == null) {
                    qualityList = new ArrayList<MvtsQualityDTO>();
                }

                if (qualityList != null && qualityList.size() > 0) {
                    for (int j = 0; j < qualityList.size(); j++) {
                        MvtsQualityDTO d = new MvtsQualityDTO();
                        d = qualityList.get(j);
                        if ((searchOriginIp && !d.getOrigination_ip().equals(temp.getOrigination_ip()))
                                || (searchOriginDest && !d.getOrigin_dest().equals(temp.getOrigin_dest()))
                                || (searchTermIp && !d.getTermination_ip().equals(temp.getTermination_ip()))
                                || (searchTermDest && !d.getTerm_dest().equals(temp.getTerm_dest()))) {
                            isMatch = false;
                            continue;
                        } else {
                            if (sdto.getSummaryType() > 0) {
                                if (temp.getPeriod_start() >= d.getPeriod_start()
                                        && temp.getPeriod_start() <= d.getPeriod_end()) {
                                    isMatch = true;
                                    if (temp.getDuration() > 0) {
                                        temp.setTotal_success(1);
                                        temp.setTotal_fail(0);
                                    } else {
                                        temp.setTotal_success(0);
                                        temp.setTotal_fail(1);
                                    }

                                    int dur = d.getDuration() + temp.getDuration();
                                    d.setDuration(dur);
                                    d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                    int suc = d.getTotal_success() + temp.getTotal_success();
                                    d.setTotal_success(suc);
                                    d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());

                                    if (searchOriginIp) {
                                        d.setOrigination_ip(temp.getOrigination_ip());
                                    } else {
                                        d.setOrigination_ip("");
                                    }
                                    if (searchOriginDest) {
                                        d.setOrigin_dest(temp.getOrigin_dest());
                                    } else {
                                        d.setOrigin_dest("");
                                    }

                                    if (searchTermIp) {
                                        d.setTermination_ip(temp.getTermination_ip());
                                    } else {
                                        d.setTermination_ip("");
                                    }

                                    if (searchTermDest) {
                                        d.setTerm_dest(temp.getTerm_dest());
                                    } else {
                                        d.setTerm_dest("");
                                    }

                                    break;
                                } else {
                                    isMatch = false;
                                    continue;
                                }
                            } else {
                                isMatch = true;
                                if (temp.getDuration() > 0) {
                                    temp.setTotal_success(1);
                                    temp.setTotal_fail(0);
                                } else {
                                    temp.setTotal_success(0);
                                    temp.setTotal_fail(1);
                                }

                                int dur = d.getDuration() + temp.getDuration();
                                d.setDuration(dur);
                                d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                int suc = d.getTotal_success() + temp.getTotal_success();
                                d.setTotal_success(suc);
                                d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                d.setPeriod_end(temp.getPeriod_start());
                                if (searchOriginIp) {
                                    d.setOrigination_ip(temp.getOrigination_ip());
                                } else {
                                    d.setOrigination_ip("");
                                }
                                if (searchOriginDest) {
                                    d.setOrigin_dest(temp.getOrigin_dest());
                                } else {
                                    d.setOrigin_dest("");
                                }

                                if (searchTermIp) {
                                    d.setTermination_ip(temp.getTermination_ip());
                                } else {
                                    d.setTermination_ip("");
                                }

                                if (searchTermDest) {
                                    d.setTerm_dest(temp.getTerm_dest());
                                } else {
                                    d.setTerm_dest("");
                                }
                                break;
                            }
                        }
                    }
                }
                if (!isMatch) {
                    long end = temp.getPeriod_end() + inc_time;
                    temp.setPeriod_end(end);

                    if (temp.getDuration() > 0) {
                        temp.setTotal_success(1);
                        temp.setTotal_fail(0);
                    } else {
                        temp.setTotal_success(0);
                        temp.setTotal_fail(1);
                    }
                    qualityList.add(temp);
                }
                qualityDtos.put(key, qualityList);
            }
        }
        long mapkey = sdto.getTermId();
        if (mapkey > 0 && qualityDtos.get(mapkey) != null) {
            returnedDtos.addAll(qualityDtos.get(mapkey));
        } else {
            tempDtos.addAll(qualityDtos.values());
            if (tempDtos != null && tempDtos.size() > 0) {
                for (int i = 0; i < tempDtos.size(); i++) {
                    returnedDtos.addAll(tempDtos.get(i));
                }
                // logger.debug("Temp Size-->" + tempDtos.size());
            }
        }

        if (returnedDtos != null) {
            return getMvtsDTOsSorted((ArrayList<MvtsQualityDTO>) returnedDtos.clone());
        } else {
            return returnedDtos;
        }
    }

    public ArrayList<MvtsQualityDTO> getMvtsDTOsSorted(ArrayList<MvtsQualityDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    MvtsQualityDTO dto1 = (MvtsQualityDTO) o1;
                    MvtsQualityDTO dto2 = (MvtsQualityDTO) o2;
                    if (dto1.getPeriod_start() > dto2.getPeriod_start()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public ArrayList<MvtsCdrDTO> getActiveMvtsCdrDTOList(int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "1";

        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select cdr_id,in_ani,out_ani,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,DATE_FORMAT(disconnect_time,'%d/%m/%Y %h:%i %p') as disconnect_time from mvts_cdr_000000"
                    + " where " + condition + " limit " + start + "," + end;
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {

                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setIn_ani(resultSet.getString("in_ani"));
                dto.setOut_ani(resultSet.getString("out_ani"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                    } else {
                        dto.setOriginClient_id("");
                    }
                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                    } else {
                        dto.setTermClient_id("");
                    }
                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public int getActiveTotalMvtsCDR() {

        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;

        String condition = ""; //elapsed_time >0";
        condition = "1";

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from mvts_cdr_000000 where " + condition;

            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }

    public ArrayList<MvtsCdrDTO> getClientActiveMvtsCdrDTOList(long client_id, int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "1";
        ArrayList<MvtsCdrDTO> mvtsCdrList = new ArrayList<MvtsCdrDTO>();
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        if (clientGateway != null) {
            for (int j = 0; j < clientGateway.size(); j++) {
                GatewayDTO gdto = clientGateway.get(j);
                if (gdto != null) {
                    if (j == 0) {
                        condition += " and (";
                    } else {
                        condition += " or ";
                    }
                    switch (gdto.getGateway_type()) {
                        case Constants.ORIGINATION:
                            condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.TERMINATION:
                            condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.BOTH:
                            condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                            break;
                    }
                    if (j == clientGateway.size() - 1) {
                        condition += " ) ";
                    }
                }
            }
        } else {
            return null;
        }


        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select cdr_id,in_ani,out_ani,cdr_date,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,DATE_FORMAT(connect_time,'%d/%m/%Y %h:%i %p') as connect_time,pdd,DATE_FORMAT(disconnect_time,'%d/%m/%Y %h:%i %p') as disconnect_time from mvts_cdr_000000"
                    + " where " + condition + " limit " + start + "," + end;
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            NumberFormat formatter = new DecimalFormat("#00.00");
            while (resultSet.next()) {

                MvtsCdrDTO dto = new MvtsCdrDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setIn_ani(resultSet.getString("in_ani"));
                dto.setOut_ani(resultSet.getString("out_ani"));
                dto.setCdr_date(resultSet.getString("cdr_date"));

                dto.setRemote_src_sig_address(Utils.getOnlyIP(resultSet.getString("remote_src_sig_address")));
                dto.setRemote_dst_sig_address(Utils.getOnlyIP(resultSet.getString("remote_dst_sig_address")));


                dto.setElapsed_time(resultSet.getInt("elapsed_time"));
                dto.setPdd(formatter.format(resultSet.getDouble("pdd") / 1000));

                dto.setSetup_time(resultSet.getString("setup_time"));

                if (resultSet.getString("connect_time") != null) {
                    dto.setConnect_time(resultSet.getString("connect_time"));
                } else {
                    dto.setConnect_time(resultSet.getString("disconnect_time"));
                }

                ClientDTO originCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_src_sig_address"));
                String dialNo = resultSet.getString("in_dnis");
                if (originCDto != null) {
                    if (originCDto.getClient_id() != null && originCDto.getClient_id().length() > 0) {
                        dto.setOriginClient_id(originCDto.getClient_id());
                    } else {
                        dto.setOriginClient_id("");
                    }
                    if (originCDto.getIncoming_prefix() != null && dialNo.startsWith(originCDto.getIncoming_prefix())) {
                        dto.setOriginClient_prefix(originCDto.getIncoming_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("in_dnis"), originCDto.getIncoming_prefix());
                    } else {
                        dto.setOriginClient_prefix("");
                    }
                    dto.setIn_dnis(dialNo);
                } else {
                    dto.setOriginClient_id("");
                    dto.setOriginClient_prefix("");
                    dto.setIn_dnis(dialNo);
                }
                if (dto.getIn_dnis() != null) {
                    DestCodeDTO dcDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getIn_dnis());
                    if (dcDto.getDestcode() != null) {
                        dto.setOriginDest(dcDto.getDestcode() + "(" + dcDto.getDestcode_name() + ")");
                    } else {
                        dto.setOriginDest("");
                    }

                } else {
                    dto.setOriginDest("");
                }

                ClientDTO termCDto = GatewayLoader.getInstance().getClientDTOByIp(resultSet.getString("remote_dst_sig_address"));
                dialNo = resultSet.getString("out_dnis");
                if (termCDto != null) {
                    if (termCDto.getClient_id() != null && termCDto.getClient_id().length() > 0) {
                        dto.setTermClient_id(termCDto.getClient_id());
                    } else {
                        dto.setTermClient_id("");
                    }
                    if (termCDto.getOutgoing_prefix() != null && resultSet.getString("out_dnis").startsWith(termCDto.getOutgoing_prefix())) {
                        dto.setTermClient_prefix(termCDto.getOutgoing_prefix());
                        dialNo = Utils.removePrefix(resultSet.getString("out_dnis"), termCDto.getOutgoing_prefix());
                    } else {
                        dto.setTermClient_prefix("");
                    }
                    dto.setOut_dnis(dialNo);
                } else {
                    dto.setTermClient_prefix("");
                    dto.setTermClient_id("");
                    dto.setOut_dnis(dialNo);
                }
                if (dto.getOut_dnis() != null) {
                    DestCodeDTO dcOutDto = DestCodeLoader.getInstance().getDestCodeForDialNumber(dto.getOut_dnis());
                    if (dcOutDto.getDestcode() != null) {
                        dto.setTermDest(dcOutDto.getDestcode() + "(" + dcOutDto.getDestcode_name() + ")");
                    } else {
                        dto.setTermDest("");
                    }

                } else {
                    dto.setTermDest("");
                }
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public int getClientActiveTotalMvtsCDR(long client_id) {

        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "1";
        ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
        if (clientGateway != null) {
            for (int j = 0; j < clientGateway.size(); j++) {
                GatewayDTO gdto = clientGateway.get(j);
                if (gdto != null) {
                    if (j == 0) {
                        condition += " and (";
                    } else {
                        condition += " or ";
                    }
                    switch (gdto.getGateway_type()) {
                        case Constants.ORIGINATION:
                            condition += " remote_src_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.TERMINATION:
                            condition += " remote_dst_sig_address like '" + gdto.getGateway_ip() + "%'";
                            break;
                        case Constants.BOTH:
                            condition += " (remote_dst_sig_address like '" + gdto.getGateway_ip() + "%' or remote_dst_sig_address like '" + gdto.getGateway_ip() + "%')";
                            break;
                    }
                    if (j == clientGateway.size() - 1) {
                        condition += " ) ";
                    }
                }
            }
        } else {
            return 0;
        }

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from mvts_cdr_000000 where " + condition;

            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in MvtsCdrLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }
}
