/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.mvtscdr;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Administrator
 */
public class MvtsCdrForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private String message;
    private long[] selectedIDs;
    private ArrayList<MvtsCdrDTO> mvtsCdrList;
    private long cdr_id;
    private long originId;
    private String originClient_id;
    private String origination_ip;
    private String originDestination;
    private long termId;
    private String termClient_id;
    private String termination_ip;
    private String termDestination;
    private int fromDay;
    private int fromMonth;
    private int fromYear;
    private int fromHour;
    private int fromMin;
    private int toDay;
    private int toMonth;
    private int toYear;
    private int toHour;
    private int toMin;
    private int callType;
    private String doSearch;
    private int[] summaryBy;
    private int summaryType;
    private ArrayList<MvtsQualityDTO> mvtsQualityList;

    public MvtsCdrForm() {
        doSearch = null;
        callType = 1;
        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        fromDay = day;
        fromMonth = month;
        fromYear = year;

        toDay = day;
        toMonth = month;
        toYear = year;
        toHour = 23;
        toMin = 59;
        summaryType =0;        
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList<MvtsCdrDTO> getMvtsCdrList() {
        return mvtsCdrList;
    }

    public void setMvtsCdrList(ArrayList<MvtsCdrDTO> mvtsCdrList) {
        this.mvtsCdrList = mvtsCdrList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public int getFromDay() {
        return fromDay;
    }

    public void setFromDay(int fromDay) {
        this.fromDay = fromDay;
    }

    public int getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(int fromMonth) {
        this.fromMonth = fromMonth;
    }

    public int getFromYear() {
        return fromYear;
    }

    public void setFromYear(int fromYear) {
        this.fromYear = fromYear;
    }

    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public int getToMonth() {
        return toMonth;
    }

    public void setToMonth(int toMonth) {
        this.toMonth = toMonth;
    }

    public int getToYear() {
        return toYear;
    }

    public void setToYear(int toYear) {
        this.toYear = toYear;
    }

    public int getFromHour() {
        return fromHour;
    }

    public void setFromHour(int fromHour) {
        this.fromHour = fromHour;
    }

    public int getFromMin() {
        return fromMin;
    }

    public void setFromMin(int fromMin) {
        this.fromMin = fromMin;
    }

    public int getToHour() {
        return toHour;
    }

    public void setToHour(int toHour) {
        this.toHour = toHour;
    }

    public int getToMin() {
        return toMin;
    }

    public void setToMin(int toMin) {
        this.toMin = toMin;
    }

    public String getOriginClient_id() {
        return originClient_id;
    }

    public void setOriginClient_id(String originClient_id) {
        this.originClient_id = originClient_id;
    }

    public String getOriginDestination() {
        return originDestination;
    }

    public void setOriginDestination(String originDestination) {
        this.originDestination = originDestination;
    }

    public String getTermClient_id() {
        return termClient_id;
    }

    public void setTermClient_id(String termClient_id) {
        this.termClient_id = termClient_id;
    }

    public String getTermDestination() {
        return termDestination;
    }

    public void setTermDestination(String termDestination) {
        this.termDestination = termDestination;
    }

    public String getOrigination_ip() {
        return origination_ip;
    }

    public void setOrigination_ip(String origination_ip) {
        this.origination_ip = origination_ip;
    }

    public String getTermination_ip() {
        return termination_ip;
    }

    public void setTermination_ip(String termination_ip) {
        this.termination_ip = termination_ip;
    }

    public long getOriginId() {
        return originId;
    }

    public void setOriginId(long originId) {
        this.originId = originId;
    }

    public long getTermId() {
        return termId;
    }

    public void setTermId(long termId) {
        this.termId = termId;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public ArrayList<MvtsQualityDTO> getMvtsQualityList() {
        return mvtsQualityList;
    }

    public void setMvtsQualityList(ArrayList<MvtsQualityDTO> mvtsQualityList) {
        this.mvtsQualityList = mvtsQualityList;
    }

    public int getSummaryType() {
        return summaryType;
    }

    public void setSummaryType(int summaryType) {
        this.summaryType = summaryType;
    }

    public int[] getSummaryBy() {
        return summaryBy;
    }

    public void setSummaryBy(int[] summaryBy) {
        this.summaryBy = summaryBy;
    }
    
}