package com.myapp.struts.rateplan;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class RateplanTaskSchedular {

    public RateplanTaskSchedular() {
    }

    public MyAppError addRateplanInformation(RateplanDTO p_dto) {
        RateplanDAO rateDAO = new RateplanDAO();
        return rateDAO.addRateplanInformation(p_dto);
    }

    public MyAppError editRateplanInformation(RateplanDTO p_dto) {
        RateplanDAO rateDAO = new RateplanDAO();
        return rateDAO.editRateplanInformation(p_dto);
    }

    public RateplanDTO getRateplanDTO(int id) {
        return RateplanLoader.getInstance().getRateplanDTOByID(id);
    }

    public ArrayList<RateplanDTO> getRateplanDTOsSorted(LoginDTO l_dto) {
        RateplanDAO rateDAO = new RateplanDAO();
        ArrayList<RateplanDTO> list = RateplanLoader.getInstance().getRateplanDTOList();
        if (list != null) {
            return rateDAO.getRateplanDTOsSorted((ArrayList<RateplanDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<RateplanDTO> getRateplanDTOs(LoginDTO l_dto) {
        return RateplanLoader.getInstance().getRateplanDTOList();
    }

    public ArrayList<RateplanDTO> getRateplanDTOsWithSearchParam(RateplanDTO udto, LoginDTO l_dto) {
        RateplanDAO rateDAO = new RateplanDAO();
        ArrayList<RateplanDTO> list = RateplanLoader.getInstance().getRateplanDTOList();
        if (list != null) {
            return rateDAO.getRateplanDTOsWithSearchParam((ArrayList<RateplanDTO>) list.clone(), udto);
        }
        return null;
    }

    public MyAppError deleteMultiple(long destcodeIds[]) {
        RateplanDAO dao = new RateplanDAO();
        return dao.multipleDelete(destcodeIds);
    }
}
