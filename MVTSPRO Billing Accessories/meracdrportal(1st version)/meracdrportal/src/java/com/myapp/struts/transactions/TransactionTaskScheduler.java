/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class TransactionTaskScheduler {

    public TransactionTaskScheduler() {
    }

    public ArrayList<TransactionDTO> getTransactionDTOsSorted(LoginDTO l_dto) {
        TransactionDAO transactionDAO = new TransactionDAO();
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionDTOList();
        if (list != null) {
            return transactionDAO.getTransactionDTOsSorted((ArrayList<TransactionDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<TransactionDTO> getTransactionDTOs(TransactionDTO udto,LoginDTO l_dto) {
        TransactionDAO transactionDAO = new TransactionDAO();
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionDTOList();
        if (list != null) {
            return transactionDAO.getTransactionDTOs((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public ArrayList<TransactionDTO> setTransactionSummeryList(LoginDTO l_dto) {
        return TransactionLoader.getInstance().getTransactionSummeryDTOList();
    }

    public ArrayList<TransactionDTO> getTransactionSummeryDTOsWithSearchParam(TransactionDTO udto, LoginDTO l_dto) {
        TransactionDAO transactionDAO = new TransactionDAO();
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionSummeryDTOList();
        if (list != null) {
            return transactionDAO.getTransactionDTOsWithSearchParam((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public ArrayList<TransactionDTO> getTransactionDTOsWithSearchParam(TransactionDTO udto, LoginDTO l_dto) {
        TransactionDAO transactionDAO = new TransactionDAO();
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionDTOList();
        if (list != null) {
            return transactionDAO.getTransactionDTOsWithSearchParam((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public TransactionDTO getTransactionSummeryByClient(long client_id) {
        TransactionDTO tdto = TransactionLoader.getInstance().getTransactionSummeryByClient(client_id);
        return tdto;
    }
}
