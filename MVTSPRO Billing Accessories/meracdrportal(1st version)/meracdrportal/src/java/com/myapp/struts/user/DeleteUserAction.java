/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.user;

import com.myapp.struts.clients.ClientDAO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class DeleteUserAction extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int cid = Integer.parseInt(request.getParameter("id"));
        UserTaskSchedular rt = new UserTaskSchedular();

        MyAppError error = new MyAppError();
        error = rt.deleteUser(cid);
        request.getSession(true).setAttribute(Constants.MESSAGE, "User Deleted Successfully!");

        return mapping.findForward("success");
    }
}