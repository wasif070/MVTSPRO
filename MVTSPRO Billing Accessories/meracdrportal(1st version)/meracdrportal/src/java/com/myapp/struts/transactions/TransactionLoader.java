/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class TransactionLoader {

    static Logger logger = Logger.getLogger(TransactionLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<TransactionDTO> transactionList = null;
    private HashMap<Long, TransactionDTO> summeryList = null;
    private HashMap<Long, TransactionDTO> transactionDTOByID = null;
    static TransactionLoader transactionLoader = null;

    public TransactionLoader() {
        forceReload();
    }

    public static TransactionLoader getInstance() {
        if (transactionLoader == null) {
            createClientLoader();
        }
        return transactionLoader;
    }

    private synchronized static void createClientLoader() {
        if (transactionLoader == null) {
            transactionLoader = new TransactionLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            transactionList = new ArrayList<TransactionDTO>();
            transactionDTOByID = new HashMap<Long, TransactionDTO>();
            summeryList = new HashMap<Long, TransactionDTO>();
            String sql = "select * from client_transactions";
            ResultSet resultSet = statement.executeQuery(sql);
            TransactionDTO dto = new TransactionDTO();
            while (resultSet.next()) {
                dto = new TransactionDTO();
                dto.setTransaction_id(resultSet.getLong("transaction_id"));
                dto.setClient_id(resultSet.getLong("client_id"));
                if (dto.getClient_id() > 0) {
                    ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(dto.getClient_id());
                    if (cdto != null) {
                        dto.setClient_name(cdto.getClient_id());
                        dto.setClient_full_name(cdto.getClient_name());
                    }
                }
                dto.setTransaction_recharge(resultSet.getDouble("transaction_recharge"));
                dto.setTransaction_return(resultSet.getDouble("transaction_return"));
                dto.setTransaction_receive(resultSet.getDouble("transaction_receive"));
                dto.setTransaction_date(resultSet.getLong("transaction_date"));
                dto.setTransaction_des(resultSet.getString("transaction_des"));
                dto.setUser_id(resultSet.getLong("user_id"));
                if (dto.getUser_id() > 0) {
                    UserDTO udto = UserLoader.getInstance().getUserDTOByID(dto.getUser_id());
                    if (udto != null) {
                        dto.setUser_name(udto.getUserId());
                    }
                }
                dto.setTransaction_date_string(Utils.LongToDate(dto.getTransaction_date()));
                TransactionDTO sdto = summeryList.get(dto.getClient_id());

                if (sdto != null) {
                    sdto.setTransaction_recharge(sdto.getTransaction_recharge() + dto.getTransaction_recharge());
                    sdto.setTransaction_return(sdto.getTransaction_return() + dto.getTransaction_return());
                    sdto.setTransaction_receive(sdto.getTransaction_receive() + dto.getTransaction_receive());
                    sdto.setTransaction_balance(sdto.getTransaction_balance() + dto.getTransaction_recharge() - dto.getTransaction_return() - dto.getTransaction_receive());
                } else {
                    sdto = new TransactionDTO();
                    sdto.setClient_id(dto.getClient_id());
                    sdto.setClient_name(dto.getClient_name());
                    sdto.setClient_full_name(dto.getClient_full_name());
                    sdto.setTransaction_recharge(dto.getTransaction_recharge());
                    sdto.setTransaction_return(dto.getTransaction_return());
                    sdto.setTransaction_receive(dto.getTransaction_receive());
                    sdto.setTransaction_balance(dto.getTransaction_recharge() - dto.getTransaction_return() - dto.getTransaction_receive());
                    summeryList.put(dto.getClient_id(), sdto);
                }

                transactionDTOByID.put(dto.getTransaction_id(), dto);
                transactionList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in TransactionLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<TransactionDTO> getTransactionDTOList() {
        checkForReload();
        return transactionList;
    }

    public synchronized ArrayList<TransactionDTO> getTransactionSummeryDTOList() {
        checkForReload();
        ArrayList<TransactionDTO> transSummery = new ArrayList<TransactionDTO>();
        //for (int inc = 0; inc < summeryList.size(); inc++) {
        transSummery.addAll(summeryList.values());
        //}
        return transSummery;
    }

    public synchronized TransactionDTO getTransactionDTOByID(long id) {
        checkForReload();
        return transactionDTOByID.get(id);
    }

    public synchronized ArrayList<TransactionDTO> getTransactionDTOByClient(long client_id) {
        checkForReload();
        ArrayList<TransactionDTO> transactionListByClient = new ArrayList<TransactionDTO>();
        for (int inc = 0; inc < transactionList.size(); inc++) {
            TransactionDTO dto = transactionList.get(inc);
            if (dto.getClient_id() == client_id) {
                transactionListByClient.add(dto);
            }
        }
        return transactionListByClient;
    }

    public synchronized TransactionDTO getTransactionSummeryByClient(long client_id) {
        checkForReload();
        return summeryList.get(client_id);
    }
}
