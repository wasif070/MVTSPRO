/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

import com.myapp.struts.clients.ClientDAO;
import com.myapp.struts.session.Constants;
import java.io.DataInputStream;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Administrator
 */
public class UploadDestCodeAction extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        DestCodeForm myForm = (DestCodeForm) form;

        FormFile myFile = myForm.getTheFile();
        String fileName = myFile.getFileName();
        if ((fileName == null) || fileName.indexOf("csv") == -1) {
            request.getSession(true).setAttribute(Constants.MESSAGE, "Invalid File Type!");
            return mapping.findForward("failure");
        } else {
            String thisLine;
            DataInputStream myInput = new DataInputStream(myFile.getInputStream());

            ArrayList<DestCodeDTO> dtoList = new ArrayList<DestCodeDTO>();

            while ((thisLine = myInput.readLine()) != null) {
                if (thisLine.trim().length() > 0) {
                    String strar[] = thisLine.split(",");
                    if (strar.length >= 2) {
                        DestCodeDTO dto = new DestCodeDTO();
                        dto.setDestcode(strar[0].trim());
                        dto.setDestcode_name(strar[1].trim());
                        dtoList.add(dto);
                    }
                }
            }
            DestCodeTaskSchedular scheduler = new DestCodeTaskSchedular();
            scheduler.uploadFlieDestCode(dtoList);
            request.getSession(true).setAttribute(Constants.MESSAGE, "Destination Code Uploaded Successfully!");
        }
        return mapping.findForward("success");
    }
}
