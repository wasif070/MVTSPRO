package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;

public class GatewayLoader {

    static Logger logger = Logger.getLogger(GatewayLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<GatewayDTO> gatewayList = null;
    private HashMap<Long, GatewayDTO> gatewayDTOByID = null;
    static GatewayLoader gatewayLoader = null;

    public GatewayLoader() {
        forceReload();
    }

    public static GatewayLoader getInstance() {
        if (gatewayLoader == null) {
            createGatewayLoader();
        }
        return gatewayLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (gatewayLoader == null) {
            gatewayLoader = new GatewayLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
                                  
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            gatewayList = new ArrayList<GatewayDTO>();
            gatewayDTOByID = new HashMap<Long, GatewayDTO>();

            String sql = "select id,gateway_ip,gateway_name,gateway_type,gateway_status,clientId from gateway where gateway_delete=0";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                GatewayDTO dto = new GatewayDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setGateway_ip(resultSet.getString("gateway_ip"));
                dto.setGateway_name(resultSet.getString("gateway_name"));
                dto.setGateway_type(resultSet.getInt("gateway_type"));
                dto.setGateway_type_name(Constants.CLIENT_TYPE_NAME[dto.getGateway_type()]);
                dto.setClientId(resultSet.getInt("clientId"));
                if (dto.getClientId() > 0) {
                    ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(resultSet.getLong("clientId"));
                    if (cdto != null) {
                        if (cdto.getClient_id() != null && cdto.getClient_id().length() > 0) {
                            dto.setClient_name(cdto.getClient_id());
                        }
                    }

                }
                dto.setGateway_status(resultSet.getInt("gateway_status"));
                dto.setGateway_status_name(Constants.GATEWAY_STATUS_STRING[dto.getGateway_status()]);
                dto.setSuperUser(false);
                gatewayDTOByID.put(dto.getId(), dto);
                gatewayList.add(dto);
                
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOList() {
        checkForReload();
        return gatewayList;
    }

    public synchronized GatewayDTO getGatewayDTOByID(long id) {
        checkForReload();
        return gatewayDTOByID.get(id);
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOByType(int type) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByType = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getGateway_type() == type) {
                gatewayListByType.add(dto);
            }
        }
        return gatewayListByType;
    }

    public synchronized ArrayList<GatewayDTO> getClientsGatewayList(long clientId) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByClient = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getClientId() == clientId) {
                gatewayListByClient.add(dto);
            }
        }
        return gatewayListByClient;
    }

    public synchronized ClientDTO getClientDTOByIp(String ip_address) {
        checkForReload();
        ClientDTO clientDto = new ClientDTO();
        if (ip_address == null) {
            return null;
        }
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (ip_address.startsWith(dto.getGateway_ip())) {
                clientDto = ClientLoader.getInstance().getClientDTOByID(dto.getClientId());
            }
        }
        return clientDto;
    }
}
