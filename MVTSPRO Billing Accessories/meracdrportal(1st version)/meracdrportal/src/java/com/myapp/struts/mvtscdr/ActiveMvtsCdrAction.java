/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.mvtscdr;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class ActiveMvtsCdrAction extends Action {

    static Logger logger = Logger.getLogger(ListMvtsCdrAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        
        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            int pageNo = 1;
            String links = "";
            int perPageRecord = Constants.PER_PAGE_RECORD;
            if (request.getParameter("recordPerPage") != null) {
                perPageRecord = Integer.parseInt(request.getParameter("recordPerPage"));
            }
            if (request.getParameter("pageNo") != null) {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            int pageStart = (pageNo - 1) * perPageRecord;
            MvtsCdrTaskSchedular scheduler = new MvtsCdrTaskSchedular();
            MvtsCdrForm mvtsCdrForm = (MvtsCdrForm) form;
            mvtsCdrForm.setMvtsCdrList(scheduler.getActiveMvtsCdrDTOs(login_dto, pageStart, perPageRecord));
            if (mvtsCdrForm.getMvtsCdrList() != null && mvtsCdrForm.getMvtsCdrList().size() > 0) {
                request.getSession(true).setAttribute("MvtsCdrDTO", mvtsCdrForm.getMvtsCdrList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("MvtsCdrDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}