/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Administrator
 */
public class DestCodeForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int id;
    private String destcode;
    private String destcode_name;
    private String message;
    private ArrayList destcodeList;
    private FormFile theFile;
    int action;
    private long[] selectedIDs;
    private String deleteBtn;

    public DestCodeForm() {
        action = 0;
    }

    public String getDestcode() {
        return destcode;
    }

    public void setDestcode(String destcode) {
        this.destcode = destcode;
    }

    public ArrayList getDestcodeList() {
        return destcodeList;
    }

    public void setDestcodeList(ArrayList destcodeList) {
        this.destcodeList = destcodeList;
    }

    public String getDestcode_name() {
        return destcode_name;
    }

    public void setDestcode_name(String destcode_name) {
        this.destcode_name = destcode_name;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }
   
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getAction() == Constants.ADD || getAction() == Constants.EDIT || getAction()==0) {
            if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
                if (getDestcode() == null || getDestcode().length() < 1) {
                    errors.add("destcode", new ActionMessage("errors.destcode.required"));
                }

                if (getDestcode_name() == null || getDestcode_name().length() < 1) {
                    errors.add("destcode_name", new ActionMessage("errors.destcode_name.required"));
                }
            }
        }
        if(getAction()==Constants.UPLOAD){
            if(getTheFile().getFileName()==null || getTheFile().getFileSize()==0){
                errors.add("theFile", new ActionMessage("errors.theFile.required"));
            }
        }

        return errors;
    }
}
