/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class test {

    public static void main(String[] args) throws Exception {
        String today = "2007-12-07 10:30:20";

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = formatter.parse(today);
        long dateInLong = date.getTime();

        System.out.println("date = " + date);
        System.out.println("dateInLong = " + dateInLong);

        SimpleDateFormat sformatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String curDate = sformatter.format(new java.util.Date(dateInLong));
        System.out.println("dateInLong = " + curDate);
    }
}