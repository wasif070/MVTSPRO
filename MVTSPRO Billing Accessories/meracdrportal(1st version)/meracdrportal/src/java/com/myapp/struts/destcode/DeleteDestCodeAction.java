/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class DeleteDestCodeAction extends Action {

    static Logger logger = Logger.getLogger(DestCodeDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int cid = Integer.parseInt(request.getParameter("id"));
        DestCodeTaskSchedular rt = new DestCodeTaskSchedular();

        MyAppError error = new MyAppError();
        error = rt.deleteDestCode(cid);
        request.getSession(true).setAttribute(Constants.MESSAGE, "Dest. Code Deleted Successfully!");
        return mapping.findForward("success");
    }
}

