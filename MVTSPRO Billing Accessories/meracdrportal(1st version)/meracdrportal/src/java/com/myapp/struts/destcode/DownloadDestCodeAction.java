/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.destcode;

import com.myapp.struts.clients.ClientDAO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class DownloadDestCodeAction extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        //tell browser program going to return an application file 
        //instead of html page
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=destinationCode.csv");
        DestCodeForm formBean = (DestCodeForm) form;
        DestCodeDTO dto = new DestCodeDTO();
        DestCodeTaskSchedular schedular = new DestCodeTaskSchedular();

        dto.setDestcode(formBean.getDestcode());
        dto.setDestcode_name(formBean.getDestcode_name());

        try {
            ServletOutputStream out = response.getOutputStream();

            StringBuffer sb = schedular.getCSVString(dto);

            InputStream in =
                    new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
            int inSize = in.available();
            byte[] outputByte = new byte[inSize];
            //copy binary contect to output stream
            while (in.read(outputByte, 0, inSize) != -1) {
                out.write(outputByte, 0, inSize);
            }
            in.close();
            out.flush();
            out.close();

        } catch (Exception e) {
        }
        return null;

    }
}
